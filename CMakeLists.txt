#*****************************************************************************
# Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#******************************************************************************

cmake_minimum_required(VERSION 3.12)

project(openScenarioEngine VERSION 0.1)

file(GLOB_RECURSE
    openScenarioEngine_SRC
    CONFIGURE_DEPENDS
    FOLLOW_SYMLINKS false
    LIST_DIRECTORIES false
     "src/*.h" "src/*.cpp")

# RA Consulting OpenSCENARIO API https://github.com/RA-Consulting-GmbH/openscenario.api.test/
set(OSC_CPP_PARSER /home/developer/repositories/rac/cpp)

# MantleAPI https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api
set(MANTLE_API /home/developer/repositories/scenario_api/MantleAPI)

# units https://github.com/nholthaus/units (dependency of MantleAPI)
set(UNITS /home/developer/repositories/units)

add_executable(openScenarioEngine
    main.cpp
    ${openScenarioEngine_SRC}
    ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/impl/ApiClassImpl.cpp)

target_compile_features(openScenarioEngine PRIVATE cxx_std_17)

target_link_libraries(openScenarioEngine
    ${OSC_CPP_PARSER}/build/output/Linux_shared/Release/libOpenScenarioLib.v1_0.so
    ${OSC_CPP_PARSER}/build/output/Linux_shared/Release/libantlr4-runtime.so)

target_include_directories(openScenarioEngine PRIVATE
                           ${UNITS}/include
                           ${MANTLE_API}/include
                           ${CMAKE_CURRENT_LIST_DIR}/src
                           ${CMAKE_CURRENT_LIST_DIR}/src/generated
                           ${CMAKE_CURRENT_LIST_DIR}/src/implementation
                           ${CMAKE_CURRENT_LIST_DIR}/src/Common
                           ${OSC_CPP_PARSER}
                           ${OSC_CPP_PARSER}/..
                           ${OSC_CPP_PARSER}/common
                           ${OSC_CPP_PARSER}/externalLibs/Filesystem
                           ${OSC_CPP_PARSER}/externalLibs/TinyXML2
                           ${OSC_CPP_PARSER}/openScenarioLib/common/api
                           ${OSC_CPP_PARSER}/openScenarioLib/common/checker
                           ${OSC_CPP_PARSER}/openScenarioLib/common/checker/tree
                           ${OSC_CPP_PARSER}/openScenarioLib/common/common
                           ${OSC_CPP_PARSER}/openScenarioLib/common/impl
                           ${OSC_CPP_PARSER}/openScenarioLib/common/loader
                           ${OSC_CPP_PARSER}/openScenarioLib/common/parameter
                           ${OSC_CPP_PARSER}/openScenarioLib/common/parser
                           ${OSC_CPP_PARSER}/openScenarioLib/common/simple/struct
                           ${OSC_CPP_PARSER}/openScenarioLib/common/xmlIndexer
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/api
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/api/writer
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/checker
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/checker/impl
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/common
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/generated/impl
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/src/catalog
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/src/checker
                           ${OSC_CPP_PARSER}/openScenarioLib/v1_0/src/parameter
                           )

find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
   message("Found ccache")
   set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
endif(CCACHE_FOUND)

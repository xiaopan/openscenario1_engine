/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "units.h"
#include "MantleAPI/Execution/i_environment.h"

struct FakeEnvironment : public mantle_api::IEnvironment
{
    units::time::second_t current_time {0.0};

    void SetDateTime(mantle_api::DateTime date_time) override
    {
        current_time = date_time.date_time;
    }

    mantle_api::DateTime GetDateTime() override
    {
        return mantle_api::DateTime{current_time};
    }

    void CreateMap(const std::string& map_file_path, const std::vector<mantle_api::Position>& map_region) override {};
    void CreateController(std::unique_ptr<mantle_api::IControllerConfig> config) override {};
    void AddEntityToController(mantle_api::IEntity& entity, mantle_api::UniqueId controller_id) override {};
    void RemoveControllerFromEntity(mantle_api::UniqueId entity_id) override {};
    void UpdateControlStrategies(
      mantle_api::UniqueId entity_id, std::vector<std::unique_ptr<mantle_api::ControlStrategy>>& control_strategies) override {};
    bool HasControlStrategyGoalBeenReached(mantle_api::UniqueId entity_id, mantle_api::ControlStrategyType type) const override {};
    const mantle_api::ILaneLocationQueryService& GetQueryService() const override {};
    const mantle_api::ICoordConverter* GetConverter() const override {};
    mantle_api::IEntityRepository& GetEntityRepository() override {};
    const mantle_api::IEntityRepository& GetEntityRepository() const override {};
    mantle_api::SimulationTime GetSimulationTime() override {};
    void SetWeather(mantle_api::Weather weather) override {};
    void SetRoadCondition(std::vector<mantle_api::FrictionPatch> friction_patches) override {};
};


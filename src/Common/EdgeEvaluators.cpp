/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EdgeEvaluators.h"

namespace OPENSCENARIO
{

void NoEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = is_satisfied;
}

void RisingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (!last_is_satisfied_ && is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

void FallingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (last_is_satisfied_ && !is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

void RisingOrFallingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (last_is_satisfied_ != is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

} // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once
#include "Common/Node.h"

namespace OPENSCENARIO
{

template <typename Element>
struct ElementWrapper
{
    Element& element;
    mutable bool complete{false};

    bool Complete() const
    {
        if (!complete)
        {
            complete = element.Complete();
        }
        return complete;
    }

    void Stop()
    {
        element.Stop();
    }

    void Step()
    {
        if (!Complete())
        {
            element.Step();
        }
    }
};

template <typename Element, typename Trigger>
class StateMachine
{
    std::vector<ElementWrapper<Element>> elements;
    Trigger* start_trigger{nullptr};
    Trigger* stop_trigger{nullptr};

    bool start_satisfied{false};
    bool stop_satisfied{false};

    auto wrap(std::vector<std::unique_ptr<Node>>& nodes)
    {
        std::vector<ElementWrapper<Element>> wrapped_elements;
        std::transform(
            nodes.begin(),
            nodes.end(),
            std::back_inserter(wrapped_elements),
            [](const auto& node) -> ElementWrapper<Element> {
                if (auto concrete_ptr = dynamic_cast<Element*>(node.get()); concrete_ptr)
                {
                    return {*concrete_ptr};
                }
                throw std::runtime_error("StateMachine: Unable two transform generic Node to specialized Element");
            });
        return wrapped_elements;
    }

  public:
    StateMachine(
        std::vector<std::unique_ptr<Node>>& elements,
        Node* start_trigger,
        Node* stop_trigger)
        : elements{wrap(elements)},
          start_trigger{dynamic_cast<Trigger*>(start_trigger)},
          stop_trigger{dynamic_cast<Trigger*>(stop_trigger)},
          start_satisfied { start_trigger == nullptr }
    {}

    void StepElements()
    {
        std::for_each(elements.begin(), elements.end(), [](auto&& element) { element.Step(); });
    }

    void StopElements()
    {
        std::for_each(elements.begin(), elements.end(), [](auto&& element) { element.Stop(); });
    }

    void Stop()
    {
        if (!stop_satisfied)
        {
            StopElements();
        }
        stop_satisfied = true;
    }

    bool Stopped()
    {
        if (stop_satisfied)
        {
            return true;
        }

        if (stop_trigger && stop_trigger->IsSatisfied())
        {
            Stop();
            return true;
        }

        return false;
    }

    void Step()
    {
        if (Stopped())
        {
            return;
        }

        if (!start_satisfied)
        {
            start_satisfied = start_trigger->IsSatisfied();
        }

        if (start_satisfied)
        {
            StepElements();
        }
    }

    bool Complete() const
    {
        return stop_satisfied || std::all_of(elements.begin(), elements.end(), [](auto&& element) { return element.Complete(); });
    }
};

}  // namespace OPENSCENARIO

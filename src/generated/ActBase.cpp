/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ActBase.h"

#include "ManeuverGroup.h"
#include "OpenScenarioEngineFactory.h"
#include "Trigger.h"

namespace OPENSCENARIO
{
ActBase::ActBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAct> act)
    : Node{"ActBase"}, startTrigger_{(std::cout << "ActBase instantiating startTrigger_" << std::endl, Builder::transform<Trigger>(act->GetStartTrigger()))}, stopTrigger_{(std::cout << "ActBase instantiating stopTrigger_" << std::endl, Builder::transform<Trigger>(act->GetStopTrigger()))}, maneuverGroups_{(std::cout << "ActBase instantiating maneuverGroups_" << std::endl, Builder::transform<ManeuverGroup, ManeuverGroups_t>(act, &NET_ASAM_OPENSCENARIO::v1_0::IAct::GetManeuverGroups))}

      ,
      statemachine_{maneuverGroups_, startTrigger_.get(), stopTrigger_.get()}
{
}

bool ActBase::Complete() const
{
    std::cout << "ActBase complete?\n";
    return statemachine_.Complete();
}

void ActBase::Step()
{
    std::cout << "ActBase step!\n";
    statemachine_.Step();
}

void ActBase::Stop()
{
    std::cout << "ActBase stop!\n";
    statemachine_.Stop();
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ActorsBase.h"

#include "EntityRef.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ActorsBase::ActorsBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IActors> actors)
    : Node{"ActorsBase"}, entityRefs_{(std::cout << "ActorsBase instantiating entityRefs_" << std::endl, Builder::transform<EntityRef, EntityRefs_t>(actors, &NET_ASAM_OPENSCENARIO::v1_0::IActors::GetEntityRefs))}

{
}

bool ActorsBase::Complete() const
{
    std::cout << "ActorsBase complete?\n";
    return OPENSCENARIO::Complete(entityRefs_);
}

void ActorsBase::Step()
{
    std::cout << "ActorsBase step!\n";
    OPENSCENARIO::Step(entityRefs_);
}

void ActorsBase::Stop()
{
    std::cout << "ActorsBase stop!\n";
    OPENSCENARIO::Stop(entityRefs_);
}

}  // namespace OPENSCENARIO

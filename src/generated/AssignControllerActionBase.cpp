/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AssignControllerActionBase.h"

#include "CatalogReference.h"
#include "Controller.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static AssignControllerActionBase::AssignControllerAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAssignControllerAction> assignControllerAction)
{
    if (auto element = assignControllerAction->GetController(); element)
    {
        return Builder::transform<Controller>(element);
    }
    if (auto element = assignControllerAction->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IAssignControllerAction");
}

AssignControllerActionBase::AssignControllerActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAssignControllerAction> assignControllerAction)
    : Node{"AssignControllerActionBase"}, assignControllerAction_{(std::cout << "AssignControllerActionBase instantiating assignControllerAction_" << std::endl, resolve_choices(assignControllerAction))}

{
}

bool AssignControllerActionBase::Complete() const
{
    std::cout << "AssignControllerActionBase complete?\n";
    return OPENSCENARIO::Complete(assignControllerAction_);
}

void AssignControllerActionBase::Step()
{
    std::cout << "AssignControllerActionBase step!\n";
    OPENSCENARIO::Step(assignControllerAction_);
}

void AssignControllerActionBase::Stop()
{
    std::cout << "AssignControllerActionBase stop!\n";
    OPENSCENARIO::Stop(assignControllerAction_);
}

}  // namespace OPENSCENARIO

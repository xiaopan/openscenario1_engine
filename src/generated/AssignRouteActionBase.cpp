/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AssignRouteActionBase.h"

#include "CatalogReference.h"
#include "OpenScenarioEngineFactory.h"
#include "Route.h"

namespace OPENSCENARIO
{
static AssignRouteActionBase::AssignRouteAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAssignRouteAction> assignRouteAction)
{
    if (auto element = assignRouteAction->GetRoute(); element)
    {
        return Builder::transform<Route>(element);
    }
    if (auto element = assignRouteAction->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IAssignRouteAction");
}

AssignRouteActionBase::AssignRouteActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAssignRouteAction> assignRouteAction)
    : Node{"AssignRouteActionBase"}, assignRouteAction_{(std::cout << "AssignRouteActionBase instantiating assignRouteAction_" << std::endl, resolve_choices(assignRouteAction))}

{
}

bool AssignRouteActionBase::Complete() const
{
    std::cout << "AssignRouteActionBase complete?\n";
    return OPENSCENARIO::Complete(assignRouteAction_);
}

void AssignRouteActionBase::Step()
{
    std::cout << "AssignRouteActionBase step!\n";
    OPENSCENARIO::Step(assignRouteAction_);
}

void AssignRouteActionBase::Stop()
{
    std::cout << "AssignRouteActionBase stop!\n";
    OPENSCENARIO::Stop(assignRouteAction_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AxlesBase.h"

#include "Axle.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
AxlesBase::AxlesBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IAxles> axles)
    : Node{"AxlesBase"}, frontAxle_{(std::cout << "AxlesBase instantiating frontAxle_" << std::endl, Builder::transform<Axle>(axles->GetFrontAxle()))}, rearAxle_{(std::cout << "AxlesBase instantiating rearAxle_" << std::endl, Builder::transform<Axle>(axles->GetRearAxle()))}, additionalAxles_{(std::cout << "AxlesBase instantiating additionalAxles_" << std::endl, Builder::transform<Axle, Axles_t>(axles, &NET_ASAM_OPENSCENARIO::v1_0::IAxles::GetAdditionalAxles))}

{
}

bool AxlesBase::Complete() const
{
    std::cout << "AxlesBase complete?\n";
    return OPENSCENARIO::Complete(frontAxle_) && OPENSCENARIO::Complete(rearAxle_) && OPENSCENARIO::Complete(additionalAxles_);
}

void AxlesBase::Step()
{
    std::cout << "AxlesBase step!\n";
    OPENSCENARIO::Step(frontAxle_);
    OPENSCENARIO::Step(rearAxle_);
    OPENSCENARIO::Step(additionalAxles_);
}

void AxlesBase::Stop()
{
    std::cout << "AxlesBase stop!\n";
    OPENSCENARIO::Stop(frontAxle_);
    OPENSCENARIO::Stop(rearAxle_);
    OPENSCENARIO::Stop(additionalAxles_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ByEntityConditionBase.h"

#include "EntityCondition.h"
#include "OpenScenarioEngineFactory.h"
#include "TriggeringEntities.h"

namespace OPENSCENARIO
{
ByEntityConditionBase::ByEntityConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IByEntityCondition> byEntityCondition,
    mantle_api::IEnvironment& environment)
    : Node{"ByEntityConditionBase"}, triggeringEntities_{(std::cout << "ByEntityConditionBase instantiating triggeringEntities_" << std::endl, Builder::transform<TriggeringEntities>(byEntityCondition->GetTriggeringEntities()))}, entityCondition_{(std::cout << "ByEntityConditionBase instantiating entityCondition_" << std::endl, Builder::transform<EntityCondition>(byEntityCondition->GetEntityCondition()))}

      ,
      environment_{environment}
{
}

bool ByEntityConditionBase::Complete() const
{
    std::cout << "ByEntityConditionBase complete?\n";
    return OPENSCENARIO::Complete(triggeringEntities_) && OPENSCENARIO::Complete(entityCondition_);
}

void ByEntityConditionBase::Step()
{
    std::cout << "ByEntityConditionBase step!\n";
    OPENSCENARIO::Step(triggeringEntities_);
    OPENSCENARIO::Step(entityCondition_);
}

void ByEntityConditionBase::Stop()
{
    std::cout << "ByEntityConditionBase stop!\n";
    OPENSCENARIO::Stop(triggeringEntities_);
    OPENSCENARIO::Stop(entityCondition_);
}

}  // namespace OPENSCENARIO

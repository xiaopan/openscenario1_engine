/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CatalogDefinitionBase.h"

#include "Catalog.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
CatalogDefinitionBase::CatalogDefinitionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICatalogDefinition> catalogDefinition)
    : Node{"CatalogDefinitionBase"}, catalog_{(std::cout << "CatalogDefinitionBase instantiating catalog_" << std::endl, Builder::transform<Catalog>(catalogDefinition->GetCatalog()))}

{
}

bool CatalogDefinitionBase::Complete() const
{
    std::cout << "CatalogDefinitionBase complete?\n";
    return OPENSCENARIO::Complete(catalog_);
}

void CatalogDefinitionBase::Step()
{
    std::cout << "CatalogDefinitionBase step!\n";
    OPENSCENARIO::Step(catalog_);
}

void CatalogDefinitionBase::Stop()
{
    std::cout << "CatalogDefinitionBase stop!\n";
    OPENSCENARIO::Stop(catalog_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CatalogLocationsBase.h"

#include "ControllerCatalogLocation.h"
#include "EnvironmentCatalogLocation.h"
#include "ManeuverCatalogLocation.h"
#include "MiscObjectCatalogLocation.h"
#include "OpenScenarioEngineFactory.h"
#include "PedestrianCatalogLocation.h"
#include "RouteCatalogLocation.h"
#include "TrajectoryCatalogLocation.h"
#include "VehicleCatalogLocation.h"

namespace OPENSCENARIO
{
CatalogLocationsBase::CatalogLocationsBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICatalogLocations> catalogLocations)
    : Node{"CatalogLocationsBase"}, vehicleCatalog_{(std::cout << "CatalogLocationsBase instantiating vehicleCatalog_" << std::endl, Builder::transform<VehicleCatalogLocation>(catalogLocations->GetVehicleCatalog()))}, controllerCatalog_{(std::cout << "CatalogLocationsBase instantiating controllerCatalog_" << std::endl, Builder::transform<ControllerCatalogLocation>(catalogLocations->GetControllerCatalog()))}, pedestrianCatalog_{(std::cout << "CatalogLocationsBase instantiating pedestrianCatalog_" << std::endl, Builder::transform<PedestrianCatalogLocation>(catalogLocations->GetPedestrianCatalog()))}, miscObjectCatalog_{(std::cout << "CatalogLocationsBase instantiating miscObjectCatalog_" << std::endl, Builder::transform<MiscObjectCatalogLocation>(catalogLocations->GetMiscObjectCatalog()))}, environmentCatalog_{(std::cout << "CatalogLocationsBase instantiating environmentCatalog_" << std::endl, Builder::transform<EnvironmentCatalogLocation>(catalogLocations->GetEnvironmentCatalog()))}, maneuverCatalog_{(std::cout << "CatalogLocationsBase instantiating maneuverCatalog_" << std::endl, Builder::transform<ManeuverCatalogLocation>(catalogLocations->GetManeuverCatalog()))}, trajectoryCatalog_{(std::cout << "CatalogLocationsBase instantiating trajectoryCatalog_" << std::endl, Builder::transform<TrajectoryCatalogLocation>(catalogLocations->GetTrajectoryCatalog()))}, routeCatalog_{(std::cout << "CatalogLocationsBase instantiating routeCatalog_" << std::endl, Builder::transform<RouteCatalogLocation>(catalogLocations->GetRouteCatalog()))}

{
}

bool CatalogLocationsBase::Complete() const
{
    std::cout << "CatalogLocationsBase complete?\n";
    return OPENSCENARIO::Complete(vehicleCatalog_) && OPENSCENARIO::Complete(controllerCatalog_) && OPENSCENARIO::Complete(pedestrianCatalog_) && OPENSCENARIO::Complete(miscObjectCatalog_) && OPENSCENARIO::Complete(environmentCatalog_) && OPENSCENARIO::Complete(maneuverCatalog_) && OPENSCENARIO::Complete(trajectoryCatalog_) && OPENSCENARIO::Complete(routeCatalog_);
}

void CatalogLocationsBase::Step()
{
    std::cout << "CatalogLocationsBase step!\n";
    OPENSCENARIO::Step(vehicleCatalog_);
    OPENSCENARIO::Step(controllerCatalog_);
    OPENSCENARIO::Step(pedestrianCatalog_);
    OPENSCENARIO::Step(miscObjectCatalog_);
    OPENSCENARIO::Step(environmentCatalog_);
    OPENSCENARIO::Step(maneuverCatalog_);
    OPENSCENARIO::Step(trajectoryCatalog_);
    OPENSCENARIO::Step(routeCatalog_);
}

void CatalogLocationsBase::Stop()
{
    std::cout << "CatalogLocationsBase stop!\n";
    OPENSCENARIO::Stop(vehicleCatalog_);
    OPENSCENARIO::Stop(controllerCatalog_);
    OPENSCENARIO::Stop(pedestrianCatalog_);
    OPENSCENARIO::Stop(miscObjectCatalog_);
    OPENSCENARIO::Stop(environmentCatalog_);
    OPENSCENARIO::Stop(maneuverCatalog_);
    OPENSCENARIO::Stop(trajectoryCatalog_);
    OPENSCENARIO::Stop(routeCatalog_);
}

}  // namespace OPENSCENARIO

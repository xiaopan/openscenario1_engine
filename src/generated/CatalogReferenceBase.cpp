/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CatalogReferenceBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterAssignment.h"

namespace OPENSCENARIO
{
CatalogReferenceBase::CatalogReferenceBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ICatalogReference> catalogReference)
    : Node{"CatalogReferenceBase"}, parameterAssignments_{(std::cout << "CatalogReferenceBase instantiating parameterAssignments_" << std::endl, Builder::transform<ParameterAssignment, ParameterAssignments_t>(catalogReference, &NET_ASAM_OPENSCENARIO::v1_0::ICatalogReference::GetParameterAssignments))}

{
}

bool CatalogReferenceBase::Complete() const
{
    std::cout << "CatalogReferenceBase complete?\n";
    return OPENSCENARIO::Complete(parameterAssignments_);
}

void CatalogReferenceBase::Step()
{
    std::cout << "CatalogReferenceBase step!\n";
    OPENSCENARIO::Step(parameterAssignments_);
}

void CatalogReferenceBase::Stop()
{
    std::cout << "CatalogReferenceBase stop!\n";
    OPENSCENARIO::Stop(parameterAssignments_);
}

}  // namespace OPENSCENARIO

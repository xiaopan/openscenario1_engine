/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ClothoidBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
ClothoidBase::ClothoidBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IClothoid> clothoid)
    : Node{"ClothoidBase"}, position_{(std::cout << "ClothoidBase instantiating position_" << std::endl, Builder::transform<Position>(clothoid->GetPosition()))}

{
}

bool ClothoidBase::Complete() const
{
    std::cout << "ClothoidBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void ClothoidBase::Step()
{
    std::cout << "ClothoidBase step!\n";
    OPENSCENARIO::Step(position_);
}

void ClothoidBase::Stop()
{
    std::cout << "ClothoidBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO

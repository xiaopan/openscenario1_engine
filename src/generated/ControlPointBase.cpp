/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ControlPointBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
ControlPointBase::ControlPointBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IControlPoint> controlPoint)
    : Node{"ControlPointBase"}, position_{(std::cout << "ControlPointBase instantiating position_" << std::endl, Builder::transform<Position>(controlPoint->GetPosition()))}

{
}

bool ControlPointBase::Complete() const
{
    std::cout << "ControlPointBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void ControlPointBase::Step()
{
    std::cout << "ControlPointBase step!\n";
    OPENSCENARIO::Step(position_);
}

void ControlPointBase::Stop()
{
    std::cout << "ControlPointBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO

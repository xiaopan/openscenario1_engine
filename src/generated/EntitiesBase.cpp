/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntitiesBase.h"

#include "EntitySelection.h"
#include "OpenScenarioEngineFactory.h"
#include "ScenarioObject.h"

namespace OPENSCENARIO
{
EntitiesBase::EntitiesBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntities> entities)
    : Node{"EntitiesBase"}, scenarioObjects_{(std::cout << "EntitiesBase instantiating scenarioObjects_" << std::endl, Builder::transform<ScenarioObject, ScenarioObjects_t>(entities, &NET_ASAM_OPENSCENARIO::v1_0::IEntities::GetScenarioObjects))}, entitySelections_{(std::cout << "EntitiesBase instantiating entitySelections_" << std::endl, Builder::transform<EntitySelection, EntitySelections_t>(entities, &NET_ASAM_OPENSCENARIO::v1_0::IEntities::GetEntitySelections))}

{
}

bool EntitiesBase::Complete() const
{
    std::cout << "EntitiesBase complete?\n";
    return OPENSCENARIO::Complete(scenarioObjects_) && OPENSCENARIO::Complete(entitySelections_);
}

void EntitiesBase::Step()
{
    std::cout << "EntitiesBase step!\n";
    OPENSCENARIO::Step(scenarioObjects_);
    OPENSCENARIO::Step(entitySelections_);
}

void EntitiesBase::Stop()
{
    std::cout << "EntitiesBase stop!\n";
    OPENSCENARIO::Stop(scenarioObjects_);
    OPENSCENARIO::Stop(entitySelections_);
}

}  // namespace OPENSCENARIO

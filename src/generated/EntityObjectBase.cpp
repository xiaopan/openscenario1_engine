/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntityObjectBase.h"

#include "CatalogReference.h"
#include "MiscObject.h"
#include "OpenScenarioEngineFactory.h"
#include "Pedestrian.h"
#include "Vehicle.h"

namespace OPENSCENARIO
{
static EntityObjectBase::EntityObject_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityObject> entityObject)
{
    if (auto element = entityObject->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    if (auto element = entityObject->GetVehicle(); element)
    {
        return Builder::transform<Vehicle>(element);
    }
    if (auto element = entityObject->GetPedestrian(); element)
    {
        return Builder::transform<Pedestrian>(element);
    }
    if (auto element = entityObject->GetMiscObject(); element)
    {
        return Builder::transform<MiscObject>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IEntityObject");
}

EntityObjectBase::EntityObjectBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityObject> entityObject)
    : Node{"EntityObjectBase"}, entityObject_{(std::cout << "EntityObjectBase instantiating entityObject_" << std::endl, resolve_choices(entityObject))}

{
}

bool EntityObjectBase::Complete() const
{
    std::cout << "EntityObjectBase complete?\n";
    return OPENSCENARIO::Complete(entityObject_);
}

void EntityObjectBase::Step()
{
    std::cout << "EntityObjectBase step!\n";
    OPENSCENARIO::Step(entityObject_);
}

void EntityObjectBase::Stop()
{
    std::cout << "EntityObjectBase stop!\n";
    OPENSCENARIO::Stop(entityObject_);
}

}  // namespace OPENSCENARIO

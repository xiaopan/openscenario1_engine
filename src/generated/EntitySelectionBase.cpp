/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EntitySelectionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "SelectedEntities.h"

namespace OPENSCENARIO
{
EntitySelectionBase::EntitySelectionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntitySelection> entitySelection)
    : Node{"EntitySelectionBase"}, members_{(std::cout << "EntitySelectionBase instantiating members_" << std::endl, Builder::transform<SelectedEntities>(entitySelection->GetMembers()))}

{
}

bool EntitySelectionBase::Complete() const
{
    std::cout << "EntitySelectionBase complete?\n";
    return OPENSCENARIO::Complete(members_);
}

void EntitySelectionBase::Step()
{
    std::cout << "EntitySelectionBase step!\n";
    OPENSCENARIO::Step(members_);
}

void EntitySelectionBase::Stop()
{
    std::cout << "EntitySelectionBase stop!\n";
    OPENSCENARIO::Stop(members_);
}

}  // namespace OPENSCENARIO

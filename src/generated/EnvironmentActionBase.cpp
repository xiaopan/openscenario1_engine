/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EnvironmentActionBase.h"

#include "CatalogReference.h"
#include "Environment.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static EnvironmentActionBase::EnvironmentAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEnvironmentAction> environmentAction)
{
    if (auto element = environmentAction->GetEnvironment(); element)
    {
        return Builder::transform<Environment>(element);
    }
    if (auto element = environmentAction->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IEnvironmentAction");
}

EnvironmentActionBase::EnvironmentActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEnvironmentAction> environmentAction)
    : Node{"EnvironmentActionBase"}, environmentAction_{(std::cout << "EnvironmentActionBase instantiating environmentAction_" << std::endl, resolve_choices(environmentAction))}

{
}

bool EnvironmentActionBase::Complete() const
{
    std::cout << "EnvironmentActionBase complete?\n";
    return OPENSCENARIO::Complete(environmentAction_);
}

void EnvironmentActionBase::Step()
{
    std::cout << "EnvironmentActionBase step!\n";
    OPENSCENARIO::Step(environmentAction_);
}

void EnvironmentActionBase::Stop()
{
    std::cout << "EnvironmentActionBase stop!\n";
    OPENSCENARIO::Stop(environmentAction_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EventBase.h"

#include "Action.h"
#include "OpenScenarioEngineFactory.h"
#include "Trigger.h"

namespace OPENSCENARIO
{
EventBase::EventBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEvent> event)
    : Node{"EventBase"}, startTrigger_{(std::cout << "EventBase instantiating startTrigger_" << std::endl, Builder::transform<Trigger>(event->GetStartTrigger()))}, actions_{(std::cout << "EventBase instantiating actions_" << std::endl, Builder::transform<Action, Actions_t>(event, &NET_ASAM_OPENSCENARIO::v1_0::IEvent::GetActions))}

      ,
      statemachine_{actions_, startTrigger_.get(), nullptr}
{
}

bool EventBase::Complete() const
{
    std::cout << "EventBase complete?\n";
    return statemachine_.Complete();
}

void EventBase::Step()
{
    std::cout << "EventBase step!\n";
    statemachine_.Step();
}

void EventBase::Stop()
{
    std::cout << "EventBase stop!\n";
    statemachine_.Stop();
}

}  // namespace OPENSCENARIO

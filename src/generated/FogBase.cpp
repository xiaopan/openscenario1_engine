/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "FogBase.h"

#include "BoundingBox.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
FogBase::FogBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IFog> fog)
    : Node{"FogBase"}, boundingBox_{(std::cout << "FogBase instantiating boundingBox_" << std::endl, Builder::transform<BoundingBox>(fog->GetBoundingBox()))}

{
}

bool FogBase::Complete() const
{
    std::cout << "FogBase complete?\n";
    return OPENSCENARIO::Complete(boundingBox_);
}

void FogBase::Step()
{
    std::cout << "FogBase step!\n";
    OPENSCENARIO::Step(boundingBox_);
}

void FogBase::Stop()
{
    std::cout << "FogBase stop!\n";
    OPENSCENARIO::Stop(boundingBox_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "FollowTrajectoryActionBase.h"

#include "CatalogReference.h"
#include "OpenScenarioEngineFactory.h"
#include "TimeReference.h"
#include "Trajectory.h"
#include "TrajectoryFollowingMode.h"

namespace OPENSCENARIO
{
FollowTrajectoryActionBase::FollowTrajectoryActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IFollowTrajectoryAction> followTrajectoryAction)
    : Node{"FollowTrajectoryActionBase"}, trajectory_{(std::cout << "FollowTrajectoryActionBase instantiating trajectory_" << std::endl, Builder::transform<Trajectory>(followTrajectoryAction->GetTrajectory()))}, catalogReference_{(std::cout << "FollowTrajectoryActionBase instantiating catalogReference_" << std::endl, Builder::transform<CatalogReference>(followTrajectoryAction->GetCatalogReference()))}, timeReference_{(std::cout << "FollowTrajectoryActionBase instantiating timeReference_" << std::endl, Builder::transform<TimeReference>(followTrajectoryAction->GetTimeReference()))}, trajectoryFollowingMode_{(std::cout << "FollowTrajectoryActionBase instantiating trajectoryFollowingMode_" << std::endl, Builder::transform<TrajectoryFollowingMode>(followTrajectoryAction->GetTrajectoryFollowingMode()))}

{
}

bool FollowTrajectoryActionBase::Complete() const
{
    std::cout << "FollowTrajectoryActionBase complete?\n";
    return OPENSCENARIO::Complete(trajectory_) && OPENSCENARIO::Complete(catalogReference_) && OPENSCENARIO::Complete(timeReference_) && OPENSCENARIO::Complete(trajectoryFollowingMode_);
}

void FollowTrajectoryActionBase::Step()
{
    std::cout << "FollowTrajectoryActionBase step!\n";
    OPENSCENARIO::Step(trajectory_);
    OPENSCENARIO::Step(catalogReference_);
    OPENSCENARIO::Step(timeReference_);
    OPENSCENARIO::Step(trajectoryFollowingMode_);
}

void FollowTrajectoryActionBase::Stop()
{
    std::cout << "FollowTrajectoryActionBase stop!\n";
    OPENSCENARIO::Stop(trajectory_);
    OPENSCENARIO::Stop(catalogReference_);
    OPENSCENARIO::Stop(timeReference_);
    OPENSCENARIO::Stop(trajectoryFollowingMode_);
}

}  // namespace OPENSCENARIO

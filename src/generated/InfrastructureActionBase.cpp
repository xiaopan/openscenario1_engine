/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "InfrastructureActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "TrafficSignalAction.h"

namespace OPENSCENARIO
{
InfrastructureActionBase::InfrastructureActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInfrastructureAction> infrastructureAction)
    : Node{"InfrastructureActionBase"}, trafficSignalAction_{(std::cout << "InfrastructureActionBase instantiating trafficSignalAction_" << std::endl, Builder::transform<TrafficSignalAction>(infrastructureAction->GetTrafficSignalAction()))}

{
}

bool InfrastructureActionBase::Complete() const
{
    std::cout << "InfrastructureActionBase complete?\n";
    return OPENSCENARIO::Complete(trafficSignalAction_);
}

void InfrastructureActionBase::Step()
{
    std::cout << "InfrastructureActionBase step!\n";
    OPENSCENARIO::Step(trafficSignalAction_);
}

void InfrastructureActionBase::Stop()
{
    std::cout << "InfrastructureActionBase stop!\n";
    OPENSCENARIO::Stop(trafficSignalAction_);
}

}  // namespace OPENSCENARIO

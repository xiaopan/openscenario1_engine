/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "InitBase.h"

#include "InitActions.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
InitBase::InitBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IInit> init)
    : Node{"InitBase"}, actions_{(std::cout << "InitBase instantiating actions_" << std::endl, Builder::transform<InitActions>(init->GetActions()))}

{
}

bool InitBase::Complete() const
{
    std::cout << "InitBase complete?\n";
    return OPENSCENARIO::Complete(actions_);
}

void InitBase::Step()
{
    std::cout << "InitBase step!\n";
    OPENSCENARIO::Step(actions_);
}

void InitBase::Stop()
{
    std::cout << "InitBase stop!\n";
    OPENSCENARIO::Stop(actions_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneChangeTargetBase.h"

#include "AbsoluteTargetLane.h"
#include "OpenScenarioEngineFactory.h"
#include "RelativeTargetLane.h"

namespace OPENSCENARIO
{
static LaneChangeTargetBase::LaneChangeTarget_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneChangeTarget> laneChangeTarget)
{
    if (auto element = laneChangeTarget->GetRelativeTargetLane(); element)
    {
        return Builder::transform<RelativeTargetLane>(element);
    }
    if (auto element = laneChangeTarget->GetAbsoluteTargetLane(); element)
    {
        return Builder::transform<AbsoluteTargetLane>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ILaneChangeTarget");
}

LaneChangeTargetBase::LaneChangeTargetBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneChangeTarget> laneChangeTarget)
    : Node{"LaneChangeTargetBase"}, laneChangeTarget_{(std::cout << "LaneChangeTargetBase instantiating laneChangeTarget_" << std::endl, resolve_choices(laneChangeTarget))}

{
}

bool LaneChangeTargetBase::Complete() const
{
    std::cout << "LaneChangeTargetBase complete?\n";
    return OPENSCENARIO::Complete(laneChangeTarget_);
}

void LaneChangeTargetBase::Step()
{
    std::cout << "LaneChangeTargetBase step!\n";
    OPENSCENARIO::Step(laneChangeTarget_);
}

void LaneChangeTargetBase::Stop()
{
    std::cout << "LaneChangeTargetBase stop!\n";
    OPENSCENARIO::Stop(laneChangeTarget_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneOffsetActionBase.h"

#include "LaneOffsetActionDynamics.h"
#include "LaneOffsetTarget.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
LaneOffsetActionBase::LaneOffsetActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILaneOffsetAction> laneOffsetAction)
    : Node{"LaneOffsetActionBase"}, laneOffsetActionDynamics_{(std::cout << "LaneOffsetActionBase instantiating laneOffsetActionDynamics_" << std::endl, Builder::transform<LaneOffsetActionDynamics>(laneOffsetAction->GetLaneOffsetActionDynamics()))}, laneOffsetTarget_{(std::cout << "LaneOffsetActionBase instantiating laneOffsetTarget_" << std::endl, Builder::transform<LaneOffsetTarget>(laneOffsetAction->GetLaneOffsetTarget()))}

{
}

bool LaneOffsetActionBase::Complete() const
{
    std::cout << "LaneOffsetActionBase complete?\n";
    return OPENSCENARIO::Complete(laneOffsetActionDynamics_) && OPENSCENARIO::Complete(laneOffsetTarget_);
}

void LaneOffsetActionBase::Step()
{
    std::cout << "LaneOffsetActionBase step!\n";
    OPENSCENARIO::Step(laneOffsetActionDynamics_);
    OPENSCENARIO::Step(laneOffsetTarget_);
}

void LaneOffsetActionBase::Stop()
{
    std::cout << "LaneOffsetActionBase stop!\n";
    OPENSCENARIO::Stop(laneOffsetActionDynamics_);
    OPENSCENARIO::Stop(laneOffsetTarget_);
}

}  // namespace OPENSCENARIO

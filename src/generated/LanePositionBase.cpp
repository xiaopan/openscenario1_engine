/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LanePositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
LanePositionBase::LanePositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILanePosition> lanePosition)
    : Node{"LanePositionBase"}, orientation_{(std::cout << "LanePositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(lanePosition->GetOrientation()))}

{
}

bool LanePositionBase::Complete() const
{
    std::cout << "LanePositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void LanePositionBase::Step()
{
    std::cout << "LanePositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void LanePositionBase::Stop()
{
    std::cout << "LanePositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO

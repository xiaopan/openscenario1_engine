/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalActionBase.h"

#include "LongitudinalDistanceAction.h"
#include "OpenScenarioEngineFactory.h"
#include "SpeedAction.h"

namespace OPENSCENARIO
{
static LongitudinalActionBase::LongitudinalAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILongitudinalAction> longitudinalAction)
{
    if (auto element = longitudinalAction->GetSpeedAction(); element)
    {
        return Builder::transform<SpeedAction>(element);
    }
    if (auto element = longitudinalAction->GetLongitudinalDistanceAction(); element)
    {
        return Builder::transform<LongitudinalDistanceAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ILongitudinalAction");
}

LongitudinalActionBase::LongitudinalActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILongitudinalAction> longitudinalAction)
    : Node{"LongitudinalActionBase"}, longitudinalAction_{(std::cout << "LongitudinalActionBase instantiating longitudinalAction_" << std::endl, resolve_choices(longitudinalAction))}

{
}

bool LongitudinalActionBase::Complete() const
{
    std::cout << "LongitudinalActionBase complete?\n";
    return OPENSCENARIO::Complete(longitudinalAction_);
}

void LongitudinalActionBase::Step()
{
    std::cout << "LongitudinalActionBase step!\n";
    OPENSCENARIO::Step(longitudinalAction_);
}

void LongitudinalActionBase::Stop()
{
    std::cout << "LongitudinalActionBase stop!\n";
    OPENSCENARIO::Stop(longitudinalAction_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LongitudinalDistanceActionBase.h"

#include "DynamicConstraints.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
LongitudinalDistanceActionBase::LongitudinalDistanceActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ILongitudinalDistanceAction> longitudinalDistanceAction)
    : Node{"LongitudinalDistanceActionBase"}, dynamicConstraints_{(std::cout << "LongitudinalDistanceActionBase instantiating dynamicConstraints_" << std::endl, Builder::transform<DynamicConstraints>(longitudinalDistanceAction->GetDynamicConstraints()))}

{
}

bool LongitudinalDistanceActionBase::Complete() const
{
    std::cout << "LongitudinalDistanceActionBase complete?\n";
    return OPENSCENARIO::Complete(dynamicConstraints_);
}

void LongitudinalDistanceActionBase::Step()
{
    std::cout << "LongitudinalDistanceActionBase step!\n";
    OPENSCENARIO::Step(dynamicConstraints_);
}

void LongitudinalDistanceActionBase::Stop()
{
    std::cout << "LongitudinalDistanceActionBase stop!\n";
    OPENSCENARIO::Stop(dynamicConstraints_);
}

}  // namespace OPENSCENARIO

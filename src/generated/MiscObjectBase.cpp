/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "MiscObjectBase.h"

#include "BoundingBox.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Properties.h"

namespace OPENSCENARIO
{
MiscObjectBase::MiscObjectBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IMiscObject> miscObject)
    : Node{"MiscObjectBase"}, boundingBox_{(std::cout << "MiscObjectBase instantiating boundingBox_" << std::endl, Builder::transform<BoundingBox>(miscObject->GetBoundingBox()))}, properties_{(std::cout << "MiscObjectBase instantiating properties_" << std::endl, Builder::transform<Properties>(miscObject->GetProperties()))}, parameterDeclarations_{(std::cout << "MiscObjectBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(miscObject, &NET_ASAM_OPENSCENARIO::v1_0::IMiscObject::GetParameterDeclarations))}

{
}

bool MiscObjectBase::Complete() const
{
    std::cout << "MiscObjectBase complete?\n";
    return OPENSCENARIO::Complete(boundingBox_) && OPENSCENARIO::Complete(properties_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void MiscObjectBase::Step()
{
    std::cout << "MiscObjectBase step!\n";
    OPENSCENARIO::Step(boundingBox_);
    OPENSCENARIO::Step(properties_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void MiscObjectBase::Stop()
{
    std::cout << "MiscObjectBase stop!\n";
    OPENSCENARIO::Stop(boundingBox_);
    OPENSCENARIO::Stop(properties_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO

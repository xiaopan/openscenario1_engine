/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideControllerValueActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "OverrideBrakeAction.h"
#include "OverrideClutchAction.h"
#include "OverrideGearAction.h"
#include "OverrideParkingBrakeAction.h"
#include "OverrideSteeringWheelAction.h"
#include "OverrideThrottleAction.h"

namespace OPENSCENARIO
{
OverrideControllerValueActionBase::OverrideControllerValueActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOverrideControllerValueAction> overrideControllerValueAction)
    : Node{"OverrideControllerValueActionBase"}, throttle_{(std::cout << "OverrideControllerValueActionBase instantiating throttle_" << std::endl, Builder::transform<OverrideThrottleAction>(overrideControllerValueAction->GetThrottle()))}, brake_{(std::cout << "OverrideControllerValueActionBase instantiating brake_" << std::endl, Builder::transform<OverrideBrakeAction>(overrideControllerValueAction->GetBrake()))}, clutch_{(std::cout << "OverrideControllerValueActionBase instantiating clutch_" << std::endl, Builder::transform<OverrideClutchAction>(overrideControllerValueAction->GetClutch()))}, parkingBrake_{(std::cout << "OverrideControllerValueActionBase instantiating parkingBrake_" << std::endl, Builder::transform<OverrideParkingBrakeAction>(overrideControllerValueAction->GetParkingBrake()))}, steeringWheel_{(std::cout << "OverrideControllerValueActionBase instantiating steeringWheel_" << std::endl, Builder::transform<OverrideSteeringWheelAction>(overrideControllerValueAction->GetSteeringWheel()))}, gear_{(std::cout << "OverrideControllerValueActionBase instantiating gear_" << std::endl, Builder::transform<OverrideGearAction>(overrideControllerValueAction->GetGear()))}

{
}

bool OverrideControllerValueActionBase::Complete() const
{
    std::cout << "OverrideControllerValueActionBase complete?\n";
    return OPENSCENARIO::Complete(throttle_) && OPENSCENARIO::Complete(brake_) && OPENSCENARIO::Complete(clutch_) && OPENSCENARIO::Complete(parkingBrake_) && OPENSCENARIO::Complete(steeringWheel_) && OPENSCENARIO::Complete(gear_);
}

void OverrideControllerValueActionBase::Step()
{
    std::cout << "OverrideControllerValueActionBase step!\n";
    OPENSCENARIO::Step(throttle_);
    OPENSCENARIO::Step(brake_);
    OPENSCENARIO::Step(clutch_);
    OPENSCENARIO::Step(parkingBrake_);
    OPENSCENARIO::Step(steeringWheel_);
    OPENSCENARIO::Step(gear_);
}

void OverrideControllerValueActionBase::Stop()
{
    std::cout << "OverrideControllerValueActionBase stop!\n";
    OPENSCENARIO::Stop(throttle_);
    OPENSCENARIO::Stop(brake_);
    OPENSCENARIO::Stop(clutch_);
    OPENSCENARIO::Stop(parkingBrake_);
    OPENSCENARIO::Stop(steeringWheel_);
    OPENSCENARIO::Stop(gear_);
}

}  // namespace OPENSCENARIO

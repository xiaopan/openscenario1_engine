/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ParameterActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterModifyAction.h"
#include "ParameterSetAction.h"

namespace OPENSCENARIO
{
static ParameterActionBase::ParameterAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IParameterAction> parameterAction)
{
    if (auto element = parameterAction->GetSetAction(); element)
    {
        return Builder::transform<ParameterSetAction>(element);
    }
    if (auto element = parameterAction->GetModifyAction(); element)
    {
        return Builder::transform<ParameterModifyAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IParameterAction");
}

ParameterActionBase::ParameterActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IParameterAction> parameterAction)
    : Node{"ParameterActionBase"}, parameterAction_{(std::cout << "ParameterActionBase instantiating parameterAction_" << std::endl, resolve_choices(parameterAction))}

{
}

bool ParameterActionBase::Complete() const
{
    std::cout << "ParameterActionBase complete?\n";
    return OPENSCENARIO::Complete(parameterAction_);
}

void ParameterActionBase::Step()
{
    std::cout << "ParameterActionBase step!\n";
    OPENSCENARIO::Step(parameterAction_);
}

void ParameterActionBase::Stop()
{
    std::cout << "ParameterActionBase stop!\n";
    OPENSCENARIO::Stop(parameterAction_);
}

}  // namespace OPENSCENARIO

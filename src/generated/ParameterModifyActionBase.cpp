/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ParameterModifyActionBase.h"

#include "ModifyRule.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
ParameterModifyActionBase::ParameterModifyActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IParameterModifyAction> parameterModifyAction)
    : Node{"ParameterModifyActionBase"}, rule_{(std::cout << "ParameterModifyActionBase instantiating rule_" << std::endl, Builder::transform<ModifyRule>(parameterModifyAction->GetRule()))}

{
}

bool ParameterModifyActionBase::Complete() const
{
    std::cout << "ParameterModifyActionBase complete?\n";
    return OPENSCENARIO::Complete(rule_);
}

void ParameterModifyActionBase::Step()
{
    std::cout << "ParameterModifyActionBase step!\n";
    OPENSCENARIO::Step(rule_);
}

void ParameterModifyActionBase::Stop()
{
    std::cout << "ParameterModifyActionBase stop!\n";
    OPENSCENARIO::Stop(rule_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PhaseBase.h"

#include "OpenScenarioEngineFactory.h"
#include "TrafficSignalState.h"

namespace OPENSCENARIO
{
PhaseBase::PhaseBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPhase> phase)
    : Node{"PhaseBase"}, trafficSignalStates_{(std::cout << "PhaseBase instantiating trafficSignalStates_" << std::endl, Builder::transform<TrafficSignalState, TrafficSignalStates_t>(phase, &NET_ASAM_OPENSCENARIO::v1_0::IPhase::GetTrafficSignalStates))}

{
}

bool PhaseBase::Complete() const
{
    std::cout << "PhaseBase complete?\n";
    return OPENSCENARIO::Complete(trafficSignalStates_);
}

void PhaseBase::Step()
{
    std::cout << "PhaseBase step!\n";
    OPENSCENARIO::Step(trafficSignalStates_);
}

void PhaseBase::Stop()
{
    std::cout << "PhaseBase stop!\n";
    OPENSCENARIO::Stop(trafficSignalStates_);
}

}  // namespace OPENSCENARIO

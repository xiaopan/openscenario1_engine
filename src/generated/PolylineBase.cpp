/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PolylineBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Vertex.h"

namespace OPENSCENARIO
{
PolylineBase::PolylineBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPolyline> polyline)
    : Node{"PolylineBase"}, vertices_{(std::cout << "PolylineBase instantiating vertices_" << std::endl, Builder::transform<Vertex, Vertexs_t>(polyline, &NET_ASAM_OPENSCENARIO::v1_0::IPolyline::GetVertices))}

{
}

bool PolylineBase::Complete() const
{
    std::cout << "PolylineBase complete?\n";
    return OPENSCENARIO::Complete(vertices_);
}

void PolylineBase::Step()
{
    std::cout << "PolylineBase step!\n";
    OPENSCENARIO::Step(vertices_);
}

void PolylineBase::Stop()
{
    std::cout << "PolylineBase stop!\n";
    OPENSCENARIO::Stop(vertices_);
}

}  // namespace OPENSCENARIO

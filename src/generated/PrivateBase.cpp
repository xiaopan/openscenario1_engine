/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PrivateBase.h"

#include "OpenScenarioEngineFactory.h"
#include "PrivateAction.h"

namespace OPENSCENARIO
{
PrivateBase::PrivateBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPrivate> private_)
    : Node{"PrivateBase"}, privateActions_{(std::cout << "PrivateBase instantiating privateActions_" << std::endl, Builder::transform<PrivateAction, PrivateActions_t>(private_, &NET_ASAM_OPENSCENARIO::v1_0::IPrivate::GetPrivateActions))}

{
}

bool PrivateBase::Complete() const
{
    std::cout << "PrivateBase complete?\n";
    return OPENSCENARIO::Complete(privateActions_);
}

void PrivateBase::Step()
{
    std::cout << "PrivateBase step!\n";
    OPENSCENARIO::Step(privateActions_);
}

void PrivateBase::Stop()
{
    std::cout << "PrivateBase stop!\n";
    OPENSCENARIO::Stop(privateActions_);
}

}  // namespace OPENSCENARIO

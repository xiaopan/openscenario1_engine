/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PropertiesBase.h"

#include "File.h"
#include "OpenScenarioEngineFactory.h"
#include "Property.h"

namespace OPENSCENARIO
{
PropertiesBase::PropertiesBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IProperties> properties)
    : Node{"PropertiesBase"}, properties_{(std::cout << "PropertiesBase instantiating properties_" << std::endl, Builder::transform<Property, Properties_t>(properties, &NET_ASAM_OPENSCENARIO::v1_0::IProperties::GetProperties))}, files_{(std::cout << "PropertiesBase instantiating files_" << std::endl, Builder::transform<File, Files_t>(properties, &NET_ASAM_OPENSCENARIO::v1_0::IProperties::GetFiles))}

{
}

bool PropertiesBase::Complete() const
{
    std::cout << "PropertiesBase complete?\n";
    return OPENSCENARIO::Complete(properties_) && OPENSCENARIO::Complete(files_);
}

void PropertiesBase::Step()
{
    std::cout << "PropertiesBase step!\n";
    OPENSCENARIO::Step(properties_);
    OPENSCENARIO::Step(files_);
}

void PropertiesBase::Stop()
{
    std::cout << "PropertiesBase stop!\n";
    OPENSCENARIO::Stop(properties_);
    OPENSCENARIO::Stop(files_);
}

}  // namespace OPENSCENARIO

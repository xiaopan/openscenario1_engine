/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ReachPositionConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
ReachPositionConditionBase::ReachPositionConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IReachPositionCondition> reachPositionCondition,
    mantle_api::IEnvironment& environment)
    : Node{"ReachPositionConditionBase"}, position_{(std::cout << "ReachPositionConditionBase instantiating position_" << std::endl, Builder::transform<Position>(reachPositionCondition->GetPosition()))}

      ,
      environment_{environment}
{
}

bool ReachPositionConditionBase::Complete() const
{
    std::cout << "ReachPositionConditionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void ReachPositionConditionBase::Step()
{
    std::cout << "ReachPositionConditionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void ReachPositionConditionBase::Stop()
{
    std::cout << "ReachPositionConditionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO

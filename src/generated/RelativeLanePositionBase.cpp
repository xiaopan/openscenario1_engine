/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RelativeLanePositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
RelativeLanePositionBase::RelativeLanePositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRelativeLanePosition> relativeLanePosition)
    : Node{"RelativeLanePositionBase"}, orientation_{(std::cout << "RelativeLanePositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(relativeLanePosition->GetOrientation()))}

{
}

bool RelativeLanePositionBase::Complete() const
{
    std::cout << "RelativeLanePositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void RelativeLanePositionBase::Step()
{
    std::cout << "RelativeLanePositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void RelativeLanePositionBase::Stop()
{
    std::cout << "RelativeLanePositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO

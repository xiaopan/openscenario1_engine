/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RelativeObjectPositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
RelativeObjectPositionBase::RelativeObjectPositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRelativeObjectPosition> relativeObjectPosition)
    : Node{"RelativeObjectPositionBase"}, orientation_{(std::cout << "RelativeObjectPositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(relativeObjectPosition->GetOrientation()))}

{
}

bool RelativeObjectPositionBase::Complete() const
{
    std::cout << "RelativeObjectPositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void RelativeObjectPositionBase::Step()
{
    std::cout << "RelativeObjectPositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void RelativeObjectPositionBase::Stop()
{
    std::cout << "RelativeObjectPositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO

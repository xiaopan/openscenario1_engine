/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Properties.h"

namespace OPENSCENARIO
{
RoadConditionBase::RoadConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoadCondition> roadCondition,
    mantle_api::IEnvironment& environment)
    : Node{"RoadConditionBase"}, properties_{(std::cout << "RoadConditionBase instantiating properties_" << std::endl, Builder::transform<Properties>(roadCondition->GetProperties()))}

      ,
      environment_{environment}
{
}

bool RoadConditionBase::Complete() const
{
    std::cout << "RoadConditionBase complete?\n";
    return OPENSCENARIO::Complete(properties_);
}

void RoadConditionBase::Step()
{
    std::cout << "RoadConditionBase step!\n";
    OPENSCENARIO::Step(properties_);
}

void RoadConditionBase::Stop()
{
    std::cout << "RoadConditionBase stop!\n";
    OPENSCENARIO::Stop(properties_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoadPositionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Orientation.h"

namespace OPENSCENARIO
{
RoadPositionBase::RoadPositionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoadPosition> roadPosition)
    : Node{"RoadPositionBase"}, orientation_{(std::cout << "RoadPositionBase instantiating orientation_" << std::endl, Builder::transform<Orientation>(roadPosition->GetOrientation()))}

{
}

bool RoadPositionBase::Complete() const
{
    std::cout << "RoadPositionBase complete?\n";
    return OPENSCENARIO::Complete(orientation_);
}

void RoadPositionBase::Step()
{
    std::cout << "RoadPositionBase step!\n";
    OPENSCENARIO::Step(orientation_);
}

void RoadPositionBase::Stop()
{
    std::cout << "RoadPositionBase stop!\n";
    OPENSCENARIO::Stop(orientation_);
}

}  // namespace OPENSCENARIO

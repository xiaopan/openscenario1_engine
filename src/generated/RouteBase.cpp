/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RouteBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Waypoint.h"

namespace OPENSCENARIO
{
RouteBase::RouteBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoute> route)
    : Node{"RouteBase"}, parameterDeclarations_{(std::cout << "RouteBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(route, &NET_ASAM_OPENSCENARIO::v1_0::IRoute::GetParameterDeclarations))}, waypoints_{(std::cout << "RouteBase instantiating waypoints_" << std::endl, Builder::transform<Waypoint, Waypoints_t>(route, &NET_ASAM_OPENSCENARIO::v1_0::IRoute::GetWaypoints))}

{
}

bool RouteBase::Complete() const
{
    std::cout << "RouteBase complete?\n";
    return OPENSCENARIO::Complete(parameterDeclarations_) && OPENSCENARIO::Complete(waypoints_);
}

void RouteBase::Step()
{
    std::cout << "RouteBase step!\n";
    OPENSCENARIO::Step(parameterDeclarations_);
    OPENSCENARIO::Step(waypoints_);
}

void RouteBase::Stop()
{
    std::cout << "RouteBase stop!\n";
    OPENSCENARIO::Stop(parameterDeclarations_);
    OPENSCENARIO::Stop(waypoints_);
}

}  // namespace OPENSCENARIO

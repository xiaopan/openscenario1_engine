/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RouteRefBase.h"

#include "CatalogReference.h"
#include "OpenScenarioEngineFactory.h"
#include "Route.h"

namespace OPENSCENARIO
{
static RouteRefBase::RouteRef_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRouteRef> routeRef)
{
    if (auto element = routeRef->GetRoute(); element)
    {
        return Builder::transform<Route>(element);
    }
    if (auto element = routeRef->GetCatalogReference(); element)
    {
        return Builder::transform<CatalogReference>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IRouteRef");
}

RouteRefBase::RouteRefBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRouteRef> routeRef)
    : Node{"RouteRefBase"}, routeRef_{(std::cout << "RouteRefBase instantiating routeRef_" << std::endl, resolve_choices(routeRef))}

{
}

bool RouteRefBase::Complete() const
{
    std::cout << "RouteRefBase complete?\n";
    return OPENSCENARIO::Complete(routeRef_);
}

void RouteRefBase::Step()
{
    std::cout << "RouteRefBase step!\n";
    OPENSCENARIO::Step(routeRef_);
}

void RouteRefBase::Stop()
{
    std::cout << "RouteRefBase stop!\n";
    OPENSCENARIO::Stop(routeRef_);
}

}  // namespace OPENSCENARIO

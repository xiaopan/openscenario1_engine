/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "RoutingActionBase.h"

#include "AcquirePositionAction.h"
#include "AssignRouteAction.h"
#include "FollowTrajectoryAction.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
static RoutingActionBase::RoutingAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoutingAction> routingAction)
{
    if (auto element = routingAction->GetAssignRouteAction(); element)
    {
        return Builder::transform<AssignRouteAction>(element);
    }
    if (auto element = routingAction->GetFollowTrajectoryAction(); element)
    {
        return Builder::transform<FollowTrajectoryAction>(element);
    }
    if (auto element = routingAction->GetAcquirePositionAction(); element)
    {
        return Builder::transform<AcquirePositionAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::IRoutingAction");
}

RoutingActionBase::RoutingActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IRoutingAction> routingAction)
    : Node{"RoutingActionBase"}, routingAction_{(std::cout << "RoutingActionBase instantiating routingAction_" << std::endl, resolve_choices(routingAction))}

{
}

bool RoutingActionBase::Complete() const
{
    std::cout << "RoutingActionBase complete?\n";
    return OPENSCENARIO::Complete(routingAction_);
}

void RoutingActionBase::Step()
{
    std::cout << "RoutingActionBase step!\n";
    OPENSCENARIO::Step(routingAction_);
}

void RoutingActionBase::Stop()
{
    std::cout << "RoutingActionBase stop!\n";
    OPENSCENARIO::Stop(routingAction_);
}

}  // namespace OPENSCENARIO

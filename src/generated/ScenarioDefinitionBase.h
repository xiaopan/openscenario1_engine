/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <OpenScenarioParser/openScenarioLib/v1_0/generated/api/ApiClassInterfaces.h>

#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "Common/Node.h"

namespace OPENSCENARIO
{
class ScenarioDefinitionBase : public Node
{
  public:
    using ParameterDeclarations_t = std::vector<std::unique_ptr<Node>>;

    explicit ScenarioDefinitionBase(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IScenarioDefinition> scenarioDefinition);
    ScenarioDefinitionBase(ScenarioDefinitionBase&&) = default;
    ~ScenarioDefinitionBase() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
  protected:
    std::unique_ptr<Node> catalogLocations_;
    std::unique_ptr<Node> roadNetwork_;
    std::unique_ptr<Node> entities_;
    std::unique_ptr<Node> storyboard_;
    ParameterDeclarations_t parameterDeclarations_;
};

}  // namespace OPENSCENARIO

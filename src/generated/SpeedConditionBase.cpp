/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SpeedConditionBase.h"

#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
SpeedConditionBase::SpeedConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ISpeedCondition> speedCondition,
    mantle_api::IEnvironment& environment)
    : Node{"SpeedConditionBase"}

      ,
      rule_{Rule::from(speedCondition)},
      environment_{environment}
{
}

}  // namespace OPENSCENARIO

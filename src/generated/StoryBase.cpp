/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StoryBase.h"

#include "Act.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"

namespace OPENSCENARIO
{
StoryBase::StoryBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IStory> story)
    : Node{"StoryBase"}, parameterDeclarations_{(std::cout << "StoryBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(story, &NET_ASAM_OPENSCENARIO::v1_0::IStory::GetParameterDeclarations))}, acts_{(std::cout << "StoryBase instantiating acts_" << std::endl, Builder::transform<Act, Acts_t>(story, &NET_ASAM_OPENSCENARIO::v1_0::IStory::GetActs))}

{
}

bool StoryBase::Complete() const
{
    std::cout << "StoryBase complete?\n";
    return OPENSCENARIO::Complete(parameterDeclarations_) && OPENSCENARIO::Complete(acts_);
}

void StoryBase::Step()
{
    std::cout << "StoryBase step!\n";
    OPENSCENARIO::Step(parameterDeclarations_);
    OPENSCENARIO::Step(acts_);
}

void StoryBase::Stop()
{
    std::cout << "StoryBase stop!\n";
    OPENSCENARIO::Stop(parameterDeclarations_);
    OPENSCENARIO::Stop(acts_);
}

}  // namespace OPENSCENARIO

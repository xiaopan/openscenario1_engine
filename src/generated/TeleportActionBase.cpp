/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TeleportActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Position.h"

namespace OPENSCENARIO
{
TeleportActionBase::TeleportActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITeleportAction> teleportAction)
    : Node{"TeleportActionBase"}, position_{(std::cout << "TeleportActionBase instantiating position_" << std::endl, Builder::transform<Position>(teleportAction->GetPosition()))}

{
}

bool TeleportActionBase::Complete() const
{
    std::cout << "TeleportActionBase complete?\n";
    return OPENSCENARIO::Complete(position_);
}

void TeleportActionBase::Step()
{
    std::cout << "TeleportActionBase step!\n";
    OPENSCENARIO::Step(position_);
}

void TeleportActionBase::Stop()
{
    std::cout << "TeleportActionBase stop!\n";
    OPENSCENARIO::Stop(position_);
}

}  // namespace OPENSCENARIO

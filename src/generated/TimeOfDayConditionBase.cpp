/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TimeOfDayConditionBase.h"

#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
TimeOfDayConditionBase::TimeOfDayConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITimeOfDayCondition> timeOfDayCondition,
    mantle_api::IEnvironment& environment)
    : Node{"TimeOfDayConditionBase"}

      ,
      rule_{Rule::from(timeOfDayCondition)},
      environment_{environment}
{
}

}  // namespace OPENSCENARIO

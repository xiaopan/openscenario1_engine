/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficActionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "TrafficSinkAction.h"
#include "TrafficSourceAction.h"
#include "TrafficSwarmAction.h"

namespace OPENSCENARIO
{
static TrafficActionBase::TrafficAction_t resolve_choices(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficAction> trafficAction)
{
    if (auto element = trafficAction->GetTrafficSourceAction(); element)
    {
        return Builder::transform<TrafficSourceAction>(element);
    }
    if (auto element = trafficAction->GetTrafficSinkAction(); element)
    {
        return Builder::transform<TrafficSinkAction>(element);
    }
    if (auto element = trafficAction->GetTrafficSwarmAction(); element)
    {
        return Builder::transform<TrafficSwarmAction>(element);
    }
    throw std::runtime_error("Corrupted openSCENARIO file: No choice made within NET_ASAM_OPENSCENARIO::v1_0::ITrafficAction");
}

TrafficActionBase::TrafficActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficAction> trafficAction)
    : Node{"TrafficActionBase"}, trafficAction_{(std::cout << "TrafficActionBase instantiating trafficAction_" << std::endl, resolve_choices(trafficAction))}

{
}

bool TrafficActionBase::Complete() const
{
    std::cout << "TrafficActionBase complete?\n";
    return OPENSCENARIO::Complete(trafficAction_);
}

void TrafficActionBase::Step()
{
    std::cout << "TrafficActionBase step!\n";
    OPENSCENARIO::Step(trafficAction_);
}

void TrafficActionBase::Stop()
{
    std::cout << "TrafficActionBase stop!\n";
    OPENSCENARIO::Stop(trafficAction_);
}

}  // namespace OPENSCENARIO

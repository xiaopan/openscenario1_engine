/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficDefinitionBase.h"

#include "ControllerDistribution.h"
#include "OpenScenarioEngineFactory.h"
#include "VehicleCategoryDistribution.h"

namespace OPENSCENARIO
{
TrafficDefinitionBase::TrafficDefinitionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficDefinition> trafficDefinition)
    : Node{"TrafficDefinitionBase"}, vehicleCategoryDistribution_{(std::cout << "TrafficDefinitionBase instantiating vehicleCategoryDistribution_" << std::endl, Builder::transform<VehicleCategoryDistribution>(trafficDefinition->GetVehicleCategoryDistribution()))}, controllerDistribution_{(std::cout << "TrafficDefinitionBase instantiating controllerDistribution_" << std::endl, Builder::transform<ControllerDistribution>(trafficDefinition->GetControllerDistribution()))}

{
}

bool TrafficDefinitionBase::Complete() const
{
    std::cout << "TrafficDefinitionBase complete?\n";
    return OPENSCENARIO::Complete(vehicleCategoryDistribution_) && OPENSCENARIO::Complete(controllerDistribution_);
}

void TrafficDefinitionBase::Step()
{
    std::cout << "TrafficDefinitionBase step!\n";
    OPENSCENARIO::Step(vehicleCategoryDistribution_);
    OPENSCENARIO::Step(controllerDistribution_);
}

void TrafficDefinitionBase::Stop()
{
    std::cout << "TrafficDefinitionBase stop!\n";
    OPENSCENARIO::Stop(vehicleCategoryDistribution_);
    OPENSCENARIO::Stop(controllerDistribution_);
}

}  // namespace OPENSCENARIO

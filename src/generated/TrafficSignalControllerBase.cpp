/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalControllerBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Phase.h"

namespace OPENSCENARIO
{
TrafficSignalControllerBase::TrafficSignalControllerBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalController> trafficSignalController)
    : Node{"TrafficSignalControllerBase"}, phases_{(std::cout << "TrafficSignalControllerBase instantiating phases_" << std::endl, Builder::transform<Phase, Phases_t>(trafficSignalController, &NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalController::GetPhases))}

{
}

bool TrafficSignalControllerBase::Complete() const
{
    std::cout << "TrafficSignalControllerBase complete?\n";
    return OPENSCENARIO::Complete(phases_);
}

void TrafficSignalControllerBase::Step()
{
    std::cout << "TrafficSignalControllerBase step!\n";
    OPENSCENARIO::Step(phases_);
}

void TrafficSignalControllerBase::Stop()
{
    std::cout << "TrafficSignalControllerBase stop!\n";
    OPENSCENARIO::Stop(phases_);
}

}  // namespace OPENSCENARIO

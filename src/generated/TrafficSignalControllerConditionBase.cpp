/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalControllerConditionBase.h"

#include "OpenScenarioEngineFactory.h"
#include "Phase.h"

namespace OPENSCENARIO
{
TrafficSignalControllerConditionBase::TrafficSignalControllerConditionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalControllerCondition> trafficSignalControllerCondition,
    mantle_api::IEnvironment& environment)
    : Node{"TrafficSignalControllerConditionBase"}, phaseRefs_{(std::cout << "TrafficSignalControllerConditionBase instantiating phaseRefs_" << std::endl, Builder::transform<Phase, Phases_t>(trafficSignalControllerCondition, &NET_ASAM_OPENSCENARIO::v1_0::ITrafficSignalControllerCondition::GetPhaseRef))}

      ,
      environment_{environment}
{
}

bool TrafficSignalControllerConditionBase::Complete() const
{
    std::cout << "TrafficSignalControllerConditionBase complete?\n";
    return OPENSCENARIO::Complete(phaseRefs_);
}

void TrafficSignalControllerConditionBase::Step()
{
    std::cout << "TrafficSignalControllerConditionBase step!\n";
    OPENSCENARIO::Step(phaseRefs_);
}

void TrafficSignalControllerConditionBase::Stop()
{
    std::cout << "TrafficSignalControllerConditionBase stop!\n";
    OPENSCENARIO::Stop(phaseRefs_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSwarmActionBase.h"

#include "CentralSwarmObject.h"
#include "OpenScenarioEngineFactory.h"
#include "TrafficDefinition.h"

namespace OPENSCENARIO
{
TrafficSwarmActionBase::TrafficSwarmActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrafficSwarmAction> trafficSwarmAction)
    : Node{"TrafficSwarmActionBase"}, centralObject_{(std::cout << "TrafficSwarmActionBase instantiating centralObject_" << std::endl, Builder::transform<CentralSwarmObject>(trafficSwarmAction->GetCentralObject()))}, trafficDefinition_{(std::cout << "TrafficSwarmActionBase instantiating trafficDefinition_" << std::endl, Builder::transform<TrafficDefinition>(trafficSwarmAction->GetTrafficDefinition()))}

{
}

bool TrafficSwarmActionBase::Complete() const
{
    std::cout << "TrafficSwarmActionBase complete?\n";
    return OPENSCENARIO::Complete(centralObject_) && OPENSCENARIO::Complete(trafficDefinition_);
}

void TrafficSwarmActionBase::Step()
{
    std::cout << "TrafficSwarmActionBase step!\n";
    OPENSCENARIO::Step(centralObject_);
    OPENSCENARIO::Step(trafficDefinition_);
}

void TrafficSwarmActionBase::Stop()
{
    std::cout << "TrafficSwarmActionBase stop!\n";
    OPENSCENARIO::Stop(centralObject_);
    OPENSCENARIO::Stop(trafficDefinition_);
}

}  // namespace OPENSCENARIO

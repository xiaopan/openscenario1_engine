/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrajectoryBase.h"

#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Shape.h"

namespace OPENSCENARIO
{
TrajectoryBase::TrajectoryBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrajectory> trajectory)
    : Node{"TrajectoryBase"}, shape_{(std::cout << "TrajectoryBase instantiating shape_" << std::endl, Builder::transform<Shape>(trajectory->GetShape()))}, parameterDeclarations_{(std::cout << "TrajectoryBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(trajectory, &NET_ASAM_OPENSCENARIO::v1_0::ITrajectory::GetParameterDeclarations))}

{
}

bool TrajectoryBase::Complete() const
{
    std::cout << "TrajectoryBase complete?\n";
    return OPENSCENARIO::Complete(shape_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void TrajectoryBase::Step()
{
    std::cout << "TrajectoryBase step!\n";
    OPENSCENARIO::Step(shape_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void TrajectoryBase::Stop()
{
    std::cout << "TrajectoryBase stop!\n";
    OPENSCENARIO::Stop(shape_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrajectoryCatalogLocationBase.h"

#include "Directory.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
TrajectoryCatalogLocationBase::TrajectoryCatalogLocationBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrajectoryCatalogLocation> trajectoryCatalogLocation)
    : Node{"TrajectoryCatalogLocationBase"}, directory_{(std::cout << "TrajectoryCatalogLocationBase instantiating directory_" << std::endl, Builder::transform<Directory>(trajectoryCatalogLocation->GetDirectory()))}

{
}

bool TrajectoryCatalogLocationBase::Complete() const
{
    std::cout << "TrajectoryCatalogLocationBase complete?\n";
    return OPENSCENARIO::Complete(directory_);
}

void TrajectoryCatalogLocationBase::Step()
{
    std::cout << "TrajectoryCatalogLocationBase step!\n";
    OPENSCENARIO::Step(directory_);
}

void TrajectoryCatalogLocationBase::Stop()
{
    std::cout << "TrajectoryCatalogLocationBase stop!\n";
    OPENSCENARIO::Stop(directory_);
}

}  // namespace OPENSCENARIO

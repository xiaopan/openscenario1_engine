/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TriggerBase.h"

#include "ConditionGroup.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
TriggerBase::TriggerBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::ITrigger> trigger,
    mantle_api::IEnvironment& environment)
    : Node{"TriggerBase"}, conditionGroups_{(std::cout << "TriggerBase instantiating conditionGroups_" << std::endl, Builder::transform<ConditionGroup, ConditionGroups_t>(trigger, &NET_ASAM_OPENSCENARIO::v1_0::ITrigger::GetConditionGroups))}

      ,
      environment_{environment}
{
}

bool TriggerBase::Complete() const
{
    std::cout << "TriggerBase complete?\n";
    return OPENSCENARIO::Complete(conditionGroups_);
}

void TriggerBase::Step()
{
    std::cout << "TriggerBase step!\n";
    OPENSCENARIO::Step(conditionGroups_);
}

void TriggerBase::Stop()
{
    std::cout << "TriggerBase stop!\n";
    OPENSCENARIO::Stop(conditionGroups_);
}

}  // namespace OPENSCENARIO

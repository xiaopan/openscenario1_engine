/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "UserDefinedActionBase.h"

#include "CustomCommandAction.h"
#include "OpenScenarioEngineFactory.h"

namespace OPENSCENARIO
{
UserDefinedActionBase::UserDefinedActionBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IUserDefinedAction> userDefinedAction)
    : Node{"UserDefinedActionBase"}, customCommandAction_{(std::cout << "UserDefinedActionBase instantiating customCommandAction_" << std::endl, Builder::transform<CustomCommandAction>(userDefinedAction->GetCustomCommandAction()))}

{
}

bool UserDefinedActionBase::Complete() const
{
    std::cout << "UserDefinedActionBase complete?\n";
    return OPENSCENARIO::Complete(customCommandAction_);
}

void UserDefinedActionBase::Step()
{
    std::cout << "UserDefinedActionBase step!\n";
    OPENSCENARIO::Step(customCommandAction_);
}

void UserDefinedActionBase::Stop()
{
    std::cout << "UserDefinedActionBase stop!\n";
    OPENSCENARIO::Stop(customCommandAction_);
}

}  // namespace OPENSCENARIO

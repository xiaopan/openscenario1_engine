/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VehicleBase.h"

#include "Axles.h"
#include "BoundingBox.h"
#include "OpenScenarioEngineFactory.h"
#include "ParameterDeclaration.h"
#include "Performance.h"
#include "Properties.h"

namespace OPENSCENARIO
{
VehicleBase::VehicleBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IVehicle> vehicle)
    : Node{"VehicleBase"}, boundingBox_{(std::cout << "VehicleBase instantiating boundingBox_" << std::endl, Builder::transform<BoundingBox>(vehicle->GetBoundingBox()))}, performance_{(std::cout << "VehicleBase instantiating performance_" << std::endl, Builder::transform<Performance>(vehicle->GetPerformance()))}, axles_{(std::cout << "VehicleBase instantiating axles_" << std::endl, Builder::transform<Axles>(vehicle->GetAxles()))}, properties_{(std::cout << "VehicleBase instantiating properties_" << std::endl, Builder::transform<Properties>(vehicle->GetProperties()))}, parameterDeclarations_{(std::cout << "VehicleBase instantiating parameterDeclarations_" << std::endl, Builder::transform<ParameterDeclaration, ParameterDeclarations_t>(vehicle, &NET_ASAM_OPENSCENARIO::v1_0::IVehicle::GetParameterDeclarations))}

{
}

bool VehicleBase::Complete() const
{
    std::cout << "VehicleBase complete?\n";
    return OPENSCENARIO::Complete(boundingBox_) && OPENSCENARIO::Complete(performance_) && OPENSCENARIO::Complete(axles_) && OPENSCENARIO::Complete(properties_) && OPENSCENARIO::Complete(parameterDeclarations_);
}

void VehicleBase::Step()
{
    std::cout << "VehicleBase step!\n";
    OPENSCENARIO::Step(boundingBox_);
    OPENSCENARIO::Step(performance_);
    OPENSCENARIO::Step(axles_);
    OPENSCENARIO::Step(properties_);
    OPENSCENARIO::Step(parameterDeclarations_);
}

void VehicleBase::Stop()
{
    std::cout << "VehicleBase stop!\n";
    OPENSCENARIO::Stop(boundingBox_);
    OPENSCENARIO::Stop(performance_);
    OPENSCENARIO::Stop(axles_);
    OPENSCENARIO::Stop(properties_);
    OPENSCENARIO::Stop(parameterDeclarations_);
}

}  // namespace OPENSCENARIO

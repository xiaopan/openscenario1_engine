/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "WeatherBase.h"

#include "Fog.h"
#include "OpenScenarioEngineFactory.h"
#include "Precipitation.h"
#include "Sun.h"

namespace OPENSCENARIO
{
WeatherBase::WeatherBase(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IWeather> weather)
    : Node{"WeatherBase"}, sun_{(std::cout << "WeatherBase instantiating sun_" << std::endl, Builder::transform<Sun>(weather->GetSun()))}, fog_{(std::cout << "WeatherBase instantiating fog_" << std::endl, Builder::transform<Fog>(weather->GetFog()))}, precipitation_{(std::cout << "WeatherBase instantiating precipitation_" << std::endl, Builder::transform<Precipitation>(weather->GetPrecipitation()))}

{
}

bool WeatherBase::Complete() const
{
    std::cout << "WeatherBase complete?\n";
    return OPENSCENARIO::Complete(sun_) && OPENSCENARIO::Complete(fog_) && OPENSCENARIO::Complete(precipitation_);
}

void WeatherBase::Step()
{
    std::cout << "WeatherBase step!\n";
    OPENSCENARIO::Step(sun_);
    OPENSCENARIO::Step(fog_);
    OPENSCENARIO::Step(precipitation_);
}

void WeatherBase::Stop()
{
    std::cout << "WeatherBase stop!\n";
    OPENSCENARIO::Stop(sun_);
    OPENSCENARIO::Stop(fog_);
    OPENSCENARIO::Stop(precipitation_);
}

}  // namespace OPENSCENARIO

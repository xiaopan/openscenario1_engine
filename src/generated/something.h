
-----------
classname: AbsoluteSpeed
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AbsoluteTargetLane
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AbsoluteTargetLaneOffset
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AbsoluteTargetSpeed
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AccelerationCondition
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AcquirePositionAction
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Act
STATEMACHINE:
declaration: OPENSCENARIO::StateMachine<(ManeuverGroup, Trigger> statemachine_
definition:  statemachine_{startTrigger_, stopTrigger_, maneuverGroups_}
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maneuverGroups
  type:                      ManeuverGroup
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  startTrigger
  type:                      Trigger
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  stopTrigger
  type:                      Trigger
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Action
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  globalAction
  type:                      GlobalAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  userDefinedAction
  type:                      UserDefinedAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  privateAction
  type:                      PrivateAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ActivateControllerAction
  property:                  lateral
  type:                      boolean
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  longitudinal
  type:                      boolean
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Actors
  property:                  selectTriggeringEntities
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRefs
  type:                      EntityRef
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: AddEntityAction
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AssignControllerAction
  property:                  controller
  type:                      Controller
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: AssignRouteAction
  property:                  route
  type:                      Route
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Axle
  property:                  maxSteering
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  positionX
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  positionZ
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trackWidth
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  wheelDiameter
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Axles
  property:                  frontAxle
  type:                      Axle
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rearAxle
  type:                      Axle
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  additionalAxles
  type:                      Axle
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: BoundingBox
  property:                  center
  type:                      Center
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dimensions
  type:                      Dimensions
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ByEntityCondition
  property:                  triggeringEntities
  type:                      TriggeringEntities
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityCondition
  type:                      EntityCondition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ByObjectType
  property:                  type
  type:                      ObjectType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ByType
  property:                  objectType
  type:                      ObjectType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ByValueCondition
  property:                  parameterCondition
  type:                      ParameterCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeOfDayCondition
  type:                      TimeOfDayCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  simulationTimeCondition
  type:                      SimulationTimeCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  storyboardElementStateCondition
  type:                      StoryboardElementStateCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  userDefinedValueCondition
  type:                      UserDefinedValueCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalCondition
  type:                      TrafficSignalCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalControllerCondition
  type:                      TrafficSignalControllerCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Catalog
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  vehicles
  type:                      Vehicle
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  controllers
  type:                      Controller
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  pedestrians
  type:                      Pedestrian
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  miscObjects
  type:                      MiscObject
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  environments
  type:                      Environment
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  maneuvers
  type:                      Maneuver
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  trajectories
  type:                      Trajectory
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  routes
  type:                      Route
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: CatalogDefinition
  property:                  catalog
  type:                      Catalog
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: CatalogLocations
  property:                  vehicleCatalog
  type:                      VehicleCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controllerCatalog
  type:                      ControllerCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  pedestrianCatalog
  type:                      PedestrianCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  miscObjectCatalog
  type:                      MiscObjectCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  environmentCatalog
  type:                      EnvironmentCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maneuverCatalog
  type:                      ManeuverCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trajectoryCatalog
  type:                      TrajectoryCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  routeCatalog
  type:                      RouteCatalogLocation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: CatalogReference
  property:                  catalogName
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entryName
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterAssignments
  type:                      ParameterAssignment
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  ref
  type:                      CatalogElement
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               true
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Center
  property:                  x
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  y
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  z
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: CentralSwarmObject
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Clothoid
  property:                  curvature
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  curvatureDot
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  length
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  startTime
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  stopTime
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: CollisionCondition
  property:                  entityRef
  type:                      EntityRef
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  byType
  type:                      ByObjectType
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Condition
  property:                  conditionEdge
  type:                      ConditionEdge
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  delay
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  byEntityCondition
  type:                      ByEntityCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  byValueCondition
  type:                      ByValueCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ConditionGroup
  property:                  conditions
  type:                      Condition
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: ControlPoint
  property:                  time
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  weight
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Controller
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  properties
  type:                      Properties
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ControllerAction
  property:                  assignControllerAction
  type:                      AssignControllerAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  overrideControllerValueAction
  type:                      OverrideControllerValueAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ControllerCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ControllerDistribution
  property:                  controllerDistributionEntries
  type:                      ControllerDistributionEntry
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: ControllerDistributionEntry
  property:                  weight
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controller
  type:                      Controller
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: CustomCommandAction
  property:                  content
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   true
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  type
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: DeleteEntityAction

-----------
classname: Dimensions
  property:                  height
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  length
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  width
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Directory
  property:                  path
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: DistanceCondition
  property:                  alongRoute
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: DynamicConstraints
  property:                  maxAcceleration
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maxDeceleration
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maxSpeed
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EndOfRoadCondition
  property:                  duration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Entities
  property:                  scenarioObjects
  type:                      ScenarioObject
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  entitySelections
  type:                      EntitySelection
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: EntityAction
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  addEntityAction
  type:                      AddEntityAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  deleteEntityAction
  type:                      DeleteEntityAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EntityCondition
  property:                  endOfRoadCondition
  type:                      EndOfRoadCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  collisionCondition
  type:                      CollisionCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  offroadCondition
  type:                      OffroadCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeHeadwayCondition
  type:                      TimeHeadwayCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeToCollisionCondition
  type:                      TimeToCollisionCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  accelerationCondition
  type:                      AccelerationCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  standStillCondition
  type:                      StandStillCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  speedCondition
  type:                      SpeedCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeSpeedCondition
  type:                      RelativeSpeedCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  traveledDistanceCondition
  type:                      TraveledDistanceCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  reachPositionCondition
  type:                      ReachPositionCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  distanceCondition
  type:                      DistanceCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeDistanceCondition
  type:                      RelativeDistanceCondition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EntityObject
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  vehicle
  type:                      Vehicle
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  pedestrian
  type:                      Pedestrian
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  miscObject
  type:                      MiscObject
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EntityRef
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EntitySelection
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  members
  type:                      SelectedEntities
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Environment
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  timeOfDay
  type:                      TimeOfDay
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  weather
  type:                      Weather
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  roadCondition
  type:                      RoadCondition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EnvironmentAction
  property:                  environment
  type:                      Environment
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: EnvironmentCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Event
STATEMACHINE:
declaration: OPENSCENARIO::StateMachine<(Action, Trigger> statemachine_
definition:  statemachine_{startTrigger_, nullptr, actions_}
  property:                  maximumExecutionCount
  type:                      unsignedInt
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  priority
  type:                      Priority
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  actions
  type:                      Action
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  startTrigger
  type:                      Trigger
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: File
  property:                  filepath
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: FileHeader
  property:                  author
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  date
  type:                      dateTime
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  description
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  revMajor
  type:                      unsignedShort
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  revMinor
  type:                      unsignedShort
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: FinalSpeed
  property:                  absoluteSpeed
  type:                      AbsoluteSpeed
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeSpeedToMaster
  type:                      RelativeSpeedToMaster
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Fog
  property:                  visualRange
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  boundingBox
  type:                      BoundingBox
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: FollowTrajectoryAction
  property:                  trajectory
  type:                      Trajectory
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeReference
  type:                      TimeReference
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trajectoryFollowingMode
  type:                      TrajectoryFollowingMode
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: GlobalAction
  property:                  environmentAction
  type:                      EnvironmentAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityAction
  type:                      EntityAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterAction
  type:                      ParameterAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  infrastructureAction
  type:                      InfrastructureAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficAction
  type:                      TrafficAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: InRoutePosition
  property:                  fromCurrentEntity
  type:                      PositionOfCurrentEntity
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  fromRoadCoordinates
  type:                      PositionInRoadCoordinates
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  fromLaneCoordinates
  type:                      PositionInLaneCoordinates
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: InfrastructureAction
  property:                  trafficSignalAction
  type:                      TrafficSignalAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Init
  property:                  actions
  type:                      InitActions
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: InitActions
  property:                  globalActions
  type:                      GlobalAction
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  userDefinedActions
  type:                      UserDefinedAction
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  privates
  type:                      Private
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: Knot
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LaneChangeAction
  property:                  targetLaneOffset
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneChangeActionDynamics
  type:                      TransitionDynamics
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneChangeTarget
  type:                      LaneChangeTarget
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LaneChangeTarget
  property:                  relativeTargetLane
  type:                      RelativeTargetLane
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  absoluteTargetLane
  type:                      AbsoluteTargetLane
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LaneOffsetAction
  property:                  continuous
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneOffsetActionDynamics
  type:                      LaneOffsetActionDynamics
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneOffsetTarget
  type:                      LaneOffsetTarget
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LaneOffsetActionDynamics
  property:                  dynamicsShape
  type:                      DynamicsShape
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maxLateralAcc
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LaneOffsetTarget
  property:                  relativeTargetLaneOffset
  type:                      RelativeTargetLaneOffset
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  absoluteTargetLaneOffset
  type:                      AbsoluteTargetLaneOffset
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LanePosition
  property:                  laneId
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  offset
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  roadId
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  s
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LateralAction
  property:                  laneChangeAction
  type:                      LaneChangeAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneOffsetAction
  type:                      LaneOffsetAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  lateralDistanceAction
  type:                      LateralDistanceAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LateralDistanceAction
  property:                  continuous
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  distance
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dynamicConstraints
  type:                      DynamicConstraints
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LongitudinalAction
  property:                  speedAction
  type:                      SpeedAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  longitudinalDistanceAction
  type:                      LongitudinalDistanceAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: LongitudinalDistanceAction
  property:                  continuous
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  distance
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeGap
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dynamicConstraints
  type:                      DynamicConstraints
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Maneuver
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  events
  type:                      Event
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: ManeuverCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ManeuverGroup
  property:                  maximumExecutionCount
  type:                      unsignedInt
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  actors
  type:                      Actors
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReferences
  type:                      CatalogReference
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  maneuvers
  type:                      Maneuver
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: MiscObject
  property:                  mass
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  miscObjectCategory
  type:                      MiscObjectCategory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  boundingBox
  type:                      BoundingBox
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  properties
  type:                      Properties
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: MiscObjectCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ModifyRule
  property:                  addValue
  type:                      ParameterAddValueRule
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  multiplyByValue
  type:                      ParameterMultiplyByValueRule
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: None

-----------
classname: Nurbs
  property:                  order
  type:                      unsignedInt
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controlPoints
  type:                      ControlPoint
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  knots
  type:                      Knot
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: ObjectController
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controller
  type:                      Controller
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OffroadCondition
  property:                  duration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OpenScenario
  property:                  fileHeader
  type:                      FileHeader
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  openScenarioCategory
  type:                      OpenScenarioCategory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OpenScenarioCategory
  property:                  scenarioDefinition
  type:                      ScenarioDefinition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogDefinition
  type:                      CatalogDefinition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Orientation
  property:                  h
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  p
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  r
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  type
  type:                      ReferenceContext
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideBrakeAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideClutchAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideControllerValueAction
  property:                  throttle
  type:                      OverrideThrottleAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  brake
  type:                      OverrideBrakeAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  clutch
  type:                      OverrideClutchAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parkingBrake
  type:                      OverrideParkingBrakeAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  steeringWheel
  type:                      OverrideSteeringWheelAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  gear
  type:                      OverrideGearAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideGearAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  number
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideParkingBrakeAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideSteeringWheelAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: OverrideThrottleAction
  property:                  active
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterAction
  property:                  parameterRef
  type:                      ParameterDeclaration
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  setAction
  type:                      ParameterSetAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  modifyAction
  type:                      ParameterModifyAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterAddValueRule
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterAssignment
  property:                  parameterRef
  type:                      ParameterDeclaration
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterCondition
  property:                  parameterRef
  type:                      ParameterDeclaration
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterDeclaration
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterType
  type:                      ParameterType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterModifyAction
  property:                  rule
  type:                      ModifyRule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterMultiplyByValueRule
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ParameterSetAction
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Pedestrian
  property:                  mass
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  model
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  pedestrianCategory
  type:                      PedestrianCategory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  boundingBox
  type:                      BoundingBox
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  properties
  type:                      Properties
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: PedestrianCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Performance
  property:                  maxAcceleration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maxDeceleration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  maxSpeed
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Phase
  property:                  duration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalStates
  type:                      TrafficSignalState
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: Polyline
  property:                  vertices
  type:                      Vertex
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: Position
  property:                  worldPosition
  type:                      WorldPosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeWorldPosition
  type:                      RelativeWorldPosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeObjectPosition
  type:                      RelativeObjectPosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  roadPosition
  type:                      RoadPosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeRoadPosition
  type:                      RelativeRoadPosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  lanePosition
  type:                      LanePosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeLanePosition
  type:                      RelativeLanePosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  routePosition
  type:                      RoutePosition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: PositionInLaneCoordinates
  property:                  laneId
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  laneOffset
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  pathS
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: PositionInRoadCoordinates
  property:                  pathS
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  t
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: PositionOfCurrentEntity
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Precipitation
  property:                  intensity
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  precipitationType
  type:                      PrecipitationType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Private
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  privateActions
  type:                      PrivateAction
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: PrivateAction
  property:                  longitudinalAction
  type:                      LongitudinalAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  lateralAction
  type:                      LateralAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  visibilityAction
  type:                      VisibilityAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  synchronizeAction
  type:                      SynchronizeAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  activateControllerAction
  type:                      ActivateControllerAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controllerAction
  type:                      ControllerAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  teleportAction
  type:                      TeleportAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  routingAction
  type:                      RoutingAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Properties
  property:                  properties
  type:                      Property
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  files
  type:                      File
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: Property
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ReachPositionCondition
  property:                  tolerance
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeDistanceCondition
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  relativeDistanceType
  type:                      RelativeDistanceType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeLanePosition
  property:                  dLane
  type:                      int
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  ds
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  offset
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeObjectPosition
  property:                  dx
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dy
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dz
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeRoadPosition
  property:                  ds
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dt
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeSpeedCondition
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeSpeedToMaster
  property:                  speedTargetValueType
  type:                      SpeedTargetValueType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeTargetLane
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      int
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeTargetLaneOffset
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeTargetSpeed
  property:                  continuous
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  speedTargetValueType
  type:                      SpeedTargetValueType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RelativeWorldPosition
  property:                  dx
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dy
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dz
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RoadCondition
  property:                  frictionScaleFactor
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  properties
  type:                      Properties
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RoadNetwork
  property:                  logicFile
  type:                      File
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  sceneGraphFile
  type:                      File
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignals
  type:                      TrafficSignalController
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------

-----------
classname: RoadPosition
  property:                  roadId
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  s
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  t
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Route
  property:                  closed
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  waypoints
  type:                      Waypoint
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: RouteCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RoutePosition
  property:                  routeRef
  type:                      RouteRef
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  orientation
  type:                      Orientation
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  inRoutePosition
  type:                      InRoutePosition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RouteRef
  property:                  route
  type:                      Route
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  catalogReference
  type:                      CatalogReference
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: RoutingAction
  property:                  assignRouteAction
  type:                      AssignRouteAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  followTrajectoryAction
  type:                      FollowTrajectoryAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  acquirePositionAction
  type:                      AcquirePositionAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ScenarioDefinition
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  catalogLocations
  type:                      CatalogLocations
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  roadNetwork
  type:                      RoadNetwork
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entities
  type:                      Entities
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  storyboard
  type:                      Storyboard
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: ScenarioObject
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityObject
  type:                      EntityObject
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  objectController
  type:                      ObjectController
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SelectedEntities
  property:                  entityRef
  type:                      EntityRef
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  byType
  type:                      ByType
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Shape
  property:                  polyline
  type:                      Polyline
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  clothoid
  type:                      Clothoid
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  nurbs
  type:                      Nurbs
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SimulationTimeCondition
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SpeedAction
  property:                  speedActionDynamics
  type:                      TransitionDynamics
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  speedActionTarget
  type:                      SpeedActionTarget
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SpeedActionTarget
  property:                  relativeTargetSpeed
  type:                      RelativeTargetSpeed
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  absoluteTargetSpeed
  type:                      AbsoluteTargetSpeed
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SpeedCondition
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: StandStillCondition
  property:                  duration
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Story
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  acts
  type:                      Act
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: Storyboard
STATEMACHINE:
declaration: OPENSCENARIO::StateMachine<(Story, Trigger> statemachine_
definition:  statemachine_{nullptr, stopTrigger_, stories_}
  property:                  init
  type:                      Init
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  stories
  type:                      Story
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------
  property:                  stopTrigger
  type:                      Trigger
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: StoryboardElementStateCondition
  property:                  state
  type:                      StoryboardElementState
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  storyboardElementRef
  type:                      StoryboardElement
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  storyboardElementType
  type:                      StoryboardElementType
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Sun
  property:                  azimuth
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  elevation
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  intensity
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: SynchronizeAction
  property:                  masterEntityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  targetPositionMaster
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  targetPosition
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  finalSpeed
  type:                      FinalSpeed
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TeleportAction
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeHeadwayCondition
  property:                  alongRoute
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      Entity
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeOfDay
  property:                  animation
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dateTime
  type:                      dateTime
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeOfDayCondition
  property:                  dateTime
  type:                      dateTime
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeReference
  property:                  none
  type:                      None
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timing
  type:                      Timing
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeToCollisionCondition
  property:                  alongRoute
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  freespace
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  timeToCollisionConditionTarget
  type:                      TimeToCollisionConditionTarget
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TimeToCollisionConditionTarget
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRef
  type:                      EntityRef
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Timing
  property:                  domainAbsoluteRelative
  type:                      ReferenceContext
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  offset
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  scale
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficAction
  property:                  trafficSourceAction
  type:                      TrafficSourceAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSinkAction
  type:                      TrafficSinkAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSwarmAction
  type:                      TrafficSwarmAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficDefinition
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  vehicleCategoryDistribution
  type:                      VehicleCategoryDistribution
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  controllerDistribution
  type:                      ControllerDistribution
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalAction
  property:                  trafficSignalControllerAction
  type:                      TrafficSignalControllerAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalStateAction
  type:                      TrafficSignalStateAction
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalCondition
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  state
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalController
  property:                  delay
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  reference
  type:                      string
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  phases
  type:                      Phase
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalControllerAction
  property:                  phase
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalControllerRef
  type:                      TrafficSignalController
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  phaseRef
  type:                      Phase
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               true
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalControllerCondition
  property:                  phase
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalControllerRef
  type:                      TrafficSignalController
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   true
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  phaseRef
  type:                      Phase
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               true
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalState
  property:                  state
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficSignalId
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSignalStateAction
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  state
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSinkAction
  property:                  radius
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rate
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficDefinition
  type:                      TrafficDefinition
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSourceAction
  property:                  radius
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rate
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  velocity
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficDefinition
  type:                      TrafficDefinition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrafficSwarmAction
  property:                  innerRadius
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  numberOfVehicles
  type:                      unsignedInt
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  offset
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  semiMajorAxis
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  semiMinorAxis
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  velocity
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  centralObject
  type:                      CentralSwarmObject
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  trafficDefinition
  type:                      TrafficDefinition
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Trajectory
  property:                  closed
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  shape
  type:                      Shape
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrajectoryCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TrajectoryFollowingMode
  property:                  followingMode
  type:                      FollowingMode
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TransitionDynamics
  property:                  dynamicsDimension
  type:                      DynamicsDimension
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  dynamicsShape
  type:                      DynamicsShape
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: TraveledDistanceCondition
  property:                  value
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Trigger
  property:                  conditionGroups
  type:                      ConditionGroup
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: TriggeringEntities
  property:                  triggeringEntitiesRule
  type:                      TriggeringEntitiesRule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  entityRefs
  type:                      EntityRef
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: UserDefinedAction
  property:                  customCommandAction
  type:                      CustomCommandAction
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: UserDefinedValueCondition
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  rule
  type:                      Rule
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  value
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Vehicle
  property:                  name
  type:                      string
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  vehicleCategory
  type:                      VehicleCategory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  parameterDeclarations
  type:                      ParameterDeclaration
  isList:                    true
  isOptional:                true
  isOptionalUnboundList:     true
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             true
  -----------
  property:                  boundingBox
  type:                      BoundingBox
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  performance
  type:                      Performance
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  axles
  type:                      Axles
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  properties
  type:                      Properties
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: VehicleCatalogLocation
  property:                  directory
  type:                      Directory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: VehicleCategoryDistribution
  property:                  vehicleCategoryDistributionEntries
  type:                      VehicleCategoryDistributionEntry
  isList:                    true
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           true
  isWrappedList:             false
  -----------

-----------
classname: VehicleCategoryDistributionEntry
  property:                  category
  type:                      VehicleCategory
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  weight
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Vertex
  property:                  time
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: VisibilityAction
  property:                  graphics
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  sensors
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  traffic
  type:                      boolean
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Waypoint
  property:                  routeStrategy
  type:                      RouteStrategy
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  position
  type:                      Position
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: Weather
  property:                  cloudState
  type:                      CloudState
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  sun
  type:                      Sun
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  fog
  type:                      Fog
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  precipitation
  type:                      Precipitation
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: false
  isPrimitiveAttribute:      false
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

-----------
classname: WorldPosition
  property:                  h
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  p
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  r
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  x
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  y
  type:                      double
  isList:                    false
  isOptional:                false
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------
  property:                  z
  type:                      double
  isList:                    false
  isOptional:                true
  isOptionalUnboundList:     false
  isParameterizableProperty: true
  isPrimitiveAttribute:      true
  isProxy:                   false
  isSimpleContentProperty:   false
  isTransient:               false
  isUnwrappedList:           false
  isWrappedList:             false
  -----------

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "ActivateControllerAction.h"

namespace OPENSCENARIO
{

bool ActivateControllerAction::Complete() const
{
    std::cout << "ActivateControllerAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void ActivateControllerAction::Step()
{
    std::cout << "ActivateControllerAction::Step() not implemented yet. Doing nothing.\n";
}

void ActivateControllerAction::Stop()
{
    std::cout << "ActivateControllerAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
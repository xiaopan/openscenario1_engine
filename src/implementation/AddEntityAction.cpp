/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AddEntityAction.h"

namespace OPENSCENARIO
{

bool AddEntityAction::Complete() const
{
    std::cout << "AddEntityAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void AddEntityAction::Step()
{
    std::cout << "AddEntityAction::Step() not implemented yet. Doing nothing.\n";
}

void AddEntityAction::Stop()
{
    std::cout << "AddEntityAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "AssignControllerAction.h"

namespace OPENSCENARIO
{

bool AssignControllerAction::Complete() const
{
    std::cout << "AssignControllerAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void AssignControllerAction::Step()
{
    std::cout << "AssignControllerAction::Step() not implemented yet. Doing nothing.\n";
}

void AssignControllerAction::Stop()
{
    std::cout << "AssignControllerAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
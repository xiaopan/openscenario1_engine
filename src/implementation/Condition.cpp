#include "Condition.h"

namespace OPENSCENARIO
{
bool Condition::IsSatisfied() const
{
    const bool condition_status = condition_->IsSatisfied();
    conditionEdge_->UpdateStatus(condition_status);
    return conditionEdge_->GetStatus();
}
}  // namespace OPENSCENARIO
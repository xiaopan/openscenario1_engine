/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/DistanceConditionBase.h"

namespace OPENSCENARIO
{
class DistanceCondition : public DistanceConditionBase
{
  public:
    explicit DistanceCondition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IDistanceCondition> parsedDistanceCondition,
        mantle_api::IEnvironment& environment)
        : DistanceConditionBase(parsedDistanceCondition, environment)
    {
    }

    bool IsSatisfied() const override;
};

}  // namespace OPENSCENARIO

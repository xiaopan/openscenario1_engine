/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/EntityConditionBase.h"

namespace OPENSCENARIO
{
class EntityCondition : public EntityConditionBase
{
  public:
    explicit EntityCondition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IEntityCondition> parsedEntityCondition,
        mantle_api::IEnvironment& environment)
        : EntityConditionBase(parsedEntityCondition, environment)
    {
    }

    bool IsSatisfied() const override;
};

}  // namespace OPENSCENARIO

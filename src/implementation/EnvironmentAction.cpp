/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EnvironmentAction.h"

namespace OPENSCENARIO
{

bool EnvironmentAction::Complete() const
{
    std::cout << "EnvironmentAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void EnvironmentAction::Step()
{
    std::cout << "EnvironmentAction::Step() not implemented yet. Doing nothing.\n";
}

void EnvironmentAction::Stop()
{
    std::cout << "EnvironmentAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "GlobalAction.h"

namespace OPENSCENARIO
{
bool GlobalAction::Complete() const
{
    return globalAction_->Complete();
}

void GlobalAction::Step()
{
    globalAction_->Step();
}

void GlobalAction::Stop()
{
    globalAction_->Stop();
}

}  // namespace OPENSCENARIO
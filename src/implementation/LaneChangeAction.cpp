/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneChangeAction.h"

namespace OPENSCENARIO
{

bool LaneChangeAction::Complete() const
{
    std::cout << "LaneChangeAction::Complete() not implemented yet. Returning false.\n";
    return false;
}

void LaneChangeAction::Step()
{
    std::cout << "LaneChangeAction::Step() not implemented yet. Doing nothing.\n";
}

void LaneChangeAction::Stop()
{
    std::cout << "LaneChangeAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "LaneOffsetAction.h"

namespace OPENSCENARIO
{

bool LaneOffsetAction::Complete() const
{
    std::cout << "LaneOffsetAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void LaneOffsetAction::Step()
{
    std::cout << "LaneOffsetAction::Step() not implemented yet. Doing nothing.\n";
}

void LaneOffsetAction::Stop()
{
    std::cout << "LaneOffsetAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
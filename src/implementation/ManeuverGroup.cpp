#include "ManeuverGroup.h"

bool OPENSCENARIO::ManeuverGroup::Complete() const
{
    return std::all_of(maneuvers_.begin(),
                       maneuvers_.end(),
                       [](const auto& m) { return m->Complete(); });
}

void OPENSCENARIO::ManeuverGroup::Step()
{
    std::for_each(maneuvers_.begin(),
                  maneuvers_.end(),
                  [](auto& m) { m->Step(); });
}

void OPENSCENARIO::ManeuverGroup::Stop()
{
    std::for_each(maneuvers_.begin(),
                  maneuvers_.end(),
                  [](auto& m) { m->Stop(); });
}

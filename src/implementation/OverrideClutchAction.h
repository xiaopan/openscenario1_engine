/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/OverrideClutchActionBase.h"

namespace OPENSCENARIO
{
class OverrideClutchAction : public OverrideClutchActionBase
{
  public:
    explicit OverrideClutchAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IOverrideClutchAction> parsedOverrideClutchAction)
        : OverrideClutchActionBase(parsedOverrideClutchAction)
    {
    }
    OverrideClutchAction(OverrideClutchAction&&) = default;
    ~OverrideClutchAction() override = default;

    bool Complete() const override;
    void Step() override;
    void Stop() override;
};

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideControllerValueAction.h"

namespace OPENSCENARIO
{

bool OverrideControllerValueAction::Complete() const
{
    std::cout << "OverrideControllerValueAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideControllerValueAction::Step()
{
    std::cout << "OverrideControllerValueAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideControllerValueAction::Stop()
{
    std::cout << "OverrideControllerValueAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
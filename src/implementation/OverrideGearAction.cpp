/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideGearAction.h"

namespace OPENSCENARIO
{
bool OverrideGearAction::Complete() const
{
    std::cout << "OverrideGearAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideGearAction::Step()
{
    std::cout << "OverrideGearAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideGearAction::Stop()
{
    std::cout << "OverrideGearAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
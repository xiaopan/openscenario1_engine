/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "OverrideThrottleAction.h"

namespace OPENSCENARIO
{

bool OverrideThrottleAction::Complete() const
{
    std::cout << "OverrideThrottleAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void OverrideThrottleAction::Step()
{
    std::cout << "OverrideThrottleAction::Step() not implemented yet. Doing nothing.\n";
}

void OverrideThrottleAction::Stop()
{
    std::cout << "OverrideThrottleAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
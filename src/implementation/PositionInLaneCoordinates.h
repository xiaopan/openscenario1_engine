/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "generated/PositionInLaneCoordinatesBase.h"

namespace OPENSCENARIO
{
class PositionInLaneCoordinates : public PositionInLaneCoordinatesBase
{
  public:
    explicit PositionInLaneCoordinates(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_0::IPositionInLaneCoordinates> parsedPositionInLaneCoordinates)
        :  PositionInLaneCoordinatesBase(parsedPositionInLaneCoordinates)
    {
    }
};

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "PrivateAction.h"

namespace OPENSCENARIO
{
bool PrivateAction::Complete() const
{
    return privateAction_->Complete();
}

void PrivateAction::Step()
{
    privateAction_->Step();
}

void PrivateAction::Stop()
{
    privateAction_->Stop();
}

}  // namespace OPENSCENARIO
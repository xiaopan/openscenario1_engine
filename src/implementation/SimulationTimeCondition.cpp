#include "SimulationTimeCondition.h"

namespace OPENSCENARIO
{
bool SimulationTimeCondition::IsSatisfied() const
{
    const auto simulation_time = environment_.GetDateTime().date_time.value() / 1000;  // todo: use units;
    return rule_.IsSatisfied(simulation_time);
}
}  // namespace OPENSCENARIO
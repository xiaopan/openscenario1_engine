/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "SpeedAction.h"

namespace OPENSCENARIO
{

bool SpeedAction::Complete() const
{
    std::cout << "SpeedAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void SpeedAction::Step()
{
    std::cout << "SpeedAction::Step() not implemented yet. Doing nothing.\n";
}

void SpeedAction::Stop()
{
    std::cout << "SpeedAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
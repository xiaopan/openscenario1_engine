/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalAction.h"

namespace OPENSCENARIO
{

bool TrafficSignalAction::Complete() const
{
    std::cout << "TrafficSignalAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSignalAction::Step()
{
    std::cout << "TrafficSignalAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSignalAction::Stop()
{
    std::cout << "TrafficSignalAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
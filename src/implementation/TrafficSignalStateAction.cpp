/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "TrafficSignalStateAction.h"

namespace OPENSCENARIO
{

bool TrafficSignalStateAction::Complete() const
{
    std::cout << "TrafficSignalStateAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void TrafficSignalStateAction::Step()
{
    std::cout << "TrafficSignalStateAction::Step() not implemented yet. Doing nothing.\n";
}

void TrafficSignalStateAction::Stop()
{
    std::cout << "TrafficSignalStateAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO
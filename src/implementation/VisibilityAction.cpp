/*******************************************************************************
 * Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VisibilityAction.h"

namespace OPENSCENARIO
{

bool VisibilityAction::Complete() const
{
    std::cout << "VisibilityAction::Complete() not implemented yet. Returning true.\n";
    return true;
}

void VisibilityAction::Step()
{
    std::cout << "VisibilityAction::Step() not implemented yet. Doing nothing.\n";
}

void VisibilityAction::Stop()
{
    std::cout << "VisibilityAction::Stop() not implemented yet. Doing nothing.\n";
}

}  // namespace OPENSCENARIO